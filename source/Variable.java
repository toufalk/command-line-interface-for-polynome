/**
 * Classe représentant une variable.
 *
 * <p>
 * Une variable est définie par son type et son contenu. Actuellement, il y a 5 types différents :
 * VOID, entier, fraction, polynôme et texte. Chaque type a sa méthode pour récupérer le contenu
 * de la variable.
 * </p>
 *
 * <p>
 * Les objets <code>Variable</code> sont des objets immutables : une fois définis, ils ne peuvent être modifiés.
 * Toutes les opérations mathématiques dessus (addition, multiplication, puissance,...)
 * devront donc créer de nouveaux objets.
 * </p>
 *
 * <p>
 * Des méthodes statiques d'addition, soustraction, multiplication, division ainsi que de toutes les opérations
 * possibles avec le langage défini sont proposés. Elles permettent de s'affranchir du type des variables.
 * </p>
 */
class Variable {

    /** Type de variable VOID. */
    public static final int TYPE_VOID = 0;

    /** Type de variable entier. */
    public static final int TYPE_ENT = 1;

    /** Type de variable fraction. */
    public static final int TYPE_FRAC = 2;

    /** Type de variable polynôme. */
    public static final int TYPE_POLY = 3;

    /** Type de variable chaîne de caractères. */
    public static final int TYPE_STR = 4;


    /** type de la variable. */
    private int type;

    /** valeur entière de la variable. */
    private int intVal;

    /** valeur fractionnaire de la variable. */
    private Fraction fracVal;

    /** valeur polynomiale de la variable. */
    private Polynome polyVal;

    /** valeur textuelle de la variable. */
    private String str;



    /**
     * Création d'une variable de type {@link #TYPE_VOID VOID}.
     */
    public Variable() {
        type = TYPE_VOID;
    }

    /**
     * Création d'une variable de type {@link #TYPE_ENT entière}.
     *
     * @param nb  valeur de la variable.
     */
    public Variable(int nb) {
        type = TYPE_ENT;
        intVal = nb;
    }

    /**
     * Création d'une variable de type {@link #TYPE_FRAC fractionnaire}.
     *
     * @param frac  valeur de la variable.
     */
    public Variable(Fraction frac) {
        type = TYPE_FRAC;
        fracVal = frac;
    }

    /**
     * Création d'une variable de type {@link #TYPE_POLY polynomiale}.
     *
     * @param poly  valeur de la variable.
     */
    public Variable(Polynome poly) {
        type = TYPE_POLY;
        polyVal = poly;
    }

    /**
     * Création d'une variable de type {@link #TYPE_STR textuelle}.
     *
     * @param msg  valeur de la variable.
     */
    public Variable(String msg) {
        type = TYPE_STR;
        str = msg;
    }



    /**
     * Retourne le type de la variable.
     *
     * @return le type de la variable.
     */
    public int getType() {
        return type;
    }

    /**
     * Retourne la valeur entière de la variable.
     *
     * @return la valeur entière de la variable.
     */
    public int getNbVal() {
        return intVal;
    }

    /**
     * Retourne la valeur fractionnaire de la variable.
     *
     * @return la valeur fractionnaire de la variable.
     */
    public Fraction getFracVal() {
        return fracVal;
    }

    /**
     * Retourne la valeur polynomiale de la variable.
     *
     * @return la valeur polynomiale de la variable.
     */
    public Polynome getPolyVal() {
        return polyVal;
    }

    /**
     * Retourne la valeur textuelle de la variable.
     *
     * @return la valeur textuelle de la variable.
     */
    public String getStrVal() {
        return str;
    }


    /**
     * Teste si la variable est nulle.
     *
     * @return <code>true</code> si la variable est nulle, <code>false</code> sinon.
     */
    public boolean estNulle() {
        if (type == TYPE_ENT) return (intVal == 0);
        if (type == TYPE_FRAC) return fracVal.estNulle();
        if (type == TYPE_POLY) return polyVal.estNul();
        if (type == TYPE_STR) return (str == null);
        return false;
    }

    

    /**
     * Additionne deux variables.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param v1  la première opérande
     * @param v2  la deuxième opérande
     *
     * @return la variable contenant la somme <code>v1 + v2</code>
     *
     * @throws AnalyseException  si les types ne permettent pas une addition
     */
    public static Variable addition(Variable v1, Variable v2) throws AnalyseException {
        int tv1 = v1.getType();
        int tv2 = v2.getType();
        switch(tv1) {
        case TYPE_ENT:
            if (tv2 == TYPE_ENT)
                return new Variable(v1.getNbVal() + v2.getNbVal());
            if (tv2 == TYPE_FRAC)
                return new Variable(Fraction.add(v2.getFracVal(), v1.getNbVal()));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.add(v2.getPolyVal(), v1.getNbVal()));
        case TYPE_FRAC:
            if (tv2 == TYPE_ENT)
                return new Variable(Fraction.add(v1.getFracVal(), v2.getNbVal()));
            if (tv2 == TYPE_FRAC)
                return new Variable(Fraction.add(v1.getFracVal(), v2.getFracVal()));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.add(v2.getPolyVal(), v1.getFracVal()));
        case TYPE_POLY:
            if (tv2 == TYPE_ENT)
                return new Variable(Polynome.add(v1.getPolyVal(), v2.getNbVal()));
            if (tv2 == TYPE_FRAC)
                return new Variable(Polynome.add(v1.getPolyVal(), v2.getFracVal()));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.add(v1.getPolyVal(), v2.getPolyVal()));
        }
        throw new AnalyseException("Erreur de types : " + v1.getSType() + " + " + v2.getSType());
    }

    
    /**
     * Soustraie deux variables.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param v1  la première opérande
     * @param v2  la deuxième opérande
     *
     * @return la variable contenant la différence <code>v1 - v2</code>
     *
     * @throws AnalyseException  si les types ne permettent pas une soustraction
     */
    public static Variable soustraction(Variable v1, Variable v2) throws AnalyseException {
        int tv1 = v1.getType();
        int tv2 = v2.getType();
        switch(tv1) {
        case TYPE_ENT:
            if (tv2 == TYPE_ENT)
                return new Variable(v1.getNbVal() - v2.getNbVal());
            if (tv2 == TYPE_FRAC)
                return new Variable(Fraction.add(Fraction.mult(v2.getFracVal(),-1), v1.getNbVal()));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.add(Polynome.mult(v2.getPolyVal(),-1), v1.getNbVal()));
        case TYPE_FRAC:
            if (tv2 == TYPE_ENT)
                return new Variable(Fraction.add(v1.getFracVal(), -v2.getNbVal()));
            if (tv2 == TYPE_FRAC)
                return new Variable(Fraction.add(v1.getFracVal(), Fraction.mult(v2.getFracVal(),-1)));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.add(v2.getPolyVal(), v1.getFracVal()));
        case TYPE_POLY:
            if (tv2 == TYPE_ENT)
                return new Variable(Polynome.add(v1.getPolyVal(), -v2.getNbVal()));
            if (tv2 == TYPE_FRAC)
                return new Variable(Polynome.add(v1.getPolyVal(), Fraction.mult(v2.getFracVal(),-1)));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.add(v1.getPolyVal(), Polynome.mult(v2.getPolyVal(),-1)));
        }
        throw new AnalyseException("Erreur de types : " + v1.getSType() + " - " + v2.getSType());
    }


    /**
     * Multiplie deux variables.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param v1  la première opérande
     * @param v2  la deuxième opérande
     *
     * @return la variable contenant le produit <code>v1 * v2</code>
     *
     * @throws AnalyseException  si les types ne permettent pas une multiplication
     */
    public static Variable multiplication(Variable v1, Variable v2) throws AnalyseException {
        int tv1 = v1.getType();
        int tv2 = v2.getType();
        switch(tv1) {
        case TYPE_ENT:
            if (tv2 == TYPE_ENT)
                return new Variable(v1.getNbVal() * v2.getNbVal());
            if (tv2 == TYPE_FRAC)
                return new Variable(Fraction.mult(v2.getFracVal(), v1.getNbVal()));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.mult(v2.getPolyVal(), v1.getNbVal()));
        case TYPE_FRAC:
            if (tv2 == TYPE_ENT)
                return new Variable(Fraction.mult(v1.getFracVal(), v2.getNbVal()));
            if (tv2 == TYPE_FRAC)
                return new Variable(Fraction.mult(v1.getFracVal(), v2.getFracVal()));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.mult(v2.getPolyVal(), v1.getFracVal()));
        case TYPE_POLY:
            if (tv2 == TYPE_ENT)
                return new Variable(Polynome.mult(v1.getPolyVal(), v2.getNbVal()));
            if (tv2 == TYPE_FRAC)
                return new Variable(Polynome.mult(v1.getPolyVal(), v2.getFracVal()));
            if (tv2 == TYPE_POLY)
                return new Variable(Polynome.mult(v1.getPolyVal(), v2.getPolyVal()));
        }
        throw new AnalyseException("Erreur de types : " + v1.getSType() + " * " + v2.getSType());
    }


    /**
     * Divise deux variables.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param v1  la première opérande
     * @param v2  la deuxième opérande
     *
     * @return la variable contenant la division <code>v1 / v2</code>
     *
     * @throws AnalyseException  si les types ne permettent pas une division
     */
    public static Variable division(Variable v1, Variable v2) throws AnalyseException {
        int tv1 = v1.getType();
        int tv2 = v2.getType();
        switch(tv2) {
        case TYPE_ENT:
            if (tv1 == TYPE_ENT)
                return new Variable(new Fraction(v1.getNbVal(),  v2.getNbVal()));
            if (tv1 == TYPE_FRAC) 
                return new Variable(Fraction.div(v1.getFracVal(), v2.getNbVal()));
            if (tv1 == TYPE_POLY)
                return new Variable(Polynome.mult(v1.getPolyVal(), new Fraction(1,v2.getNbVal())));
        case TYPE_FRAC:
            if (tv1 == TYPE_ENT)
                return new Variable(Fraction.div(v1.getNbVal(), v2.getFracVal()));
            if (tv1 == TYPE_FRAC)
                return new Variable(Fraction.div(v1.getFracVal(), v2.getFracVal()));
            if (tv2 == TYPE_POLY) {
                int num = v2.getFracVal().getNumerateur();
                int den = v2.getFracVal().getDenominateur();
                return new Variable(Polynome.mult(v1.getPolyVal(), new Fraction(den, num)));
            }
        case TYPE_POLY:
        }
        throw new AnalyseException("Erreur de types : " + v1.getSType() + " / " + v2.getSType());
    }


    /**
     * exponentiation de deux variables.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param v1  la première opérande
     * @param v2  la deuxième opérande
     *
     * @return la variable contenant la puissance <code>v1 ^ v2</code>
     *
     * @throws AnalyseException  si les types ne permettent pas une exponentiation
     */
    public static Variable puissance(Variable v1, Variable v2) throws AnalyseException {
        int tv1 = v1.getType();
        int tv2 = v2.getType();
        switch(tv2) {
        case TYPE_ENT:
            if (tv1 == TYPE_ENT)
                return new Variable(intpower(v1.getNbVal(), v2.getNbVal()));
            if (tv1 == TYPE_FRAC)
                return new Variable(Fraction.pow(v1.getFracVal(), v2.getNbVal()));
            if (tv1 == TYPE_POLY)
                return new Variable(Polynome.pow(v1.getPolyVal(), v2.getNbVal()));
        case TYPE_FRAC:
        case TYPE_POLY:
        }
        throw new AnalyseException("Erreur de types : " + v1.getSType() + " ^ " + v2.getSType());
    }


    /**
     * L'opposé d'une variable.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param v  la variable.
     *
     * @return la variable contenant l'opposé <code>-v</code>
     *
     * @throws AnalyseException  si le type ne permet pas l'opposé
     */
    public static Variable oppose(Variable v) throws AnalyseException {
        int tv = v.getType();
        if (tv == TYPE_ENT)
            return new Variable(-v.getNbVal());
        if (tv == TYPE_FRAC)
            return new Variable(Fraction.mult(v.getFracVal(), -1));
        if (tv == TYPE_POLY)
            return new Variable(Polynome.mult(v.getPolyVal(), -1));
        throw new AnalyseException("Erreur de types :  - " + v.getSType());
    }


    /**
     * Degré polynomial d'une variable.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param v  la variable
     *
     * @return la variable contenant le degré polynomial <code>DEG(v)</code>
     *
     * @throws AnalyseException  si le type ne permet pas de calculer un degré
     */
    public static Variable getDegre(Variable v) throws AnalyseException {
        int tv = v.getType();
        if ((tv == TYPE_ENT) ||
            (tv == TYPE_FRAC))
            return new Variable(0);
        if (tv == TYPE_POLY)
            return new Variable(v.getPolyVal().getDegre());
        throw new AnalyseException("Erreur de types :  deg(" + v.getSType() + ")");
    }


    /**
     * Quotient d'une division euclidienne de deux variables.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param dividande  la première opérande
     * @param diviseur  la deuxième opérande
     *
     * @return la variable contenant le quotient de la division euclidenne de<code>dividande</code> par <code>diviseur</code>.
     *
     * @throws AnalyseException  si les types ne permettent pas une division euclidienne
     */
    public static Variable getQuoDivEucl(Variable dividande, Variable diviseur) throws AnalyseException {
        if (diviseur.estNulle())
            throw new AnalyseException("Division euclidienne par zéro");
        int ta = dividande.getType();
        int tb = diviseur.getType();
        Polynome pa = dividande.getPolyVal();
        Polynome pb = diviseur.getPolyVal();
        if (ta == Variable.TYPE_ENT) pa = new Polynome(new Monome(dividande.getNbVal()));
        if (ta == Variable.TYPE_FRAC) pa = new Polynome(new Monome(dividande.getFracVal()));
        if (tb == Variable.TYPE_ENT) pb = new Polynome(new Monome(diviseur.getNbVal()));
        if (tb == Variable.TYPE_FRAC) pb = new Polynome(new Monome(diviseur.getFracVal()));
        return new Variable(Polynome.quoDivEucl(pa, pb));
    }


    /**
     * Reste d'une division euclidienne de deux variables.
     *
     * <p>
     * La méthode s'occupe de l'opération en fonction du type de variable.
     * </p>
     *
     * @param dividande  la première opérande
     * @param diviseur  la deuxième opérande
     *
     * @return la variable contenant le reste de la division euclidenne de<code>dividande</code> par <code>diviseur</code>.
     *
     * @throws AnalyseException  si les types ne permettent pas une division euclidienne
     */
    public static Variable getRemDivEucl(Variable dividande, Variable diviseur) throws AnalyseException {
        if (diviseur.estNulle())
            throw new AnalyseException("Division euclidienne par zéro");
        Polynome pa = dividande.getPolyVal();
        int ta = dividande.getType();
        if (ta == Variable.TYPE_ENT) pa = new Polynome(new Monome(dividande.getNbVal()));
        if (ta == Variable.TYPE_FRAC) pa = new Polynome(new Monome(dividande.getFracVal()));
        Polynome pb = diviseur.getPolyVal();
        int tb = diviseur.getType();
        if (tb == Variable.TYPE_ENT) pb = new Polynome(new Monome(diviseur.getNbVal()));
        if (tb == Variable.TYPE_FRAC) pb = new Polynome(new Monome(diviseur.getFracVal()));
        return new Variable(Polynome.remDivEucl(pa, pb));
    }

    
    

    /**
     * Retourne le type de la variable sous forme de chaîne de caractères.
     *
     * @return le type sous forme de chaîne de caractères.
     */
    public String getSType() {
        return getSType(type);
    }


    /**
     * Retourne le type de variable sous forme de chaîne de caractères.
     *
     * @param type  le type de variable.
     * 
     * @return le type sous forme de chaîne de caractères.
     */
    public static String getSType(int type) {
        switch(type) {
        case TYPE_VOID: return "type_void";
        case TYPE_ENT : return "type_entier";
        case TYPE_FRAC: return "type_fraction";
        case TYPE_POLY: return "type_polynome";
	case TYPE_STR : return "type_texte";
        }
        return "Erreur: type inconnu";
    }


    /**
     * Retourne la puissance entière d'un nombre.
     *
     * <p>
     * La puissance <code>n</code> doit être un entier positif ou nul.
     * Dans le cas contraire, le comportement est non spécifié et ne donne
     * pas le résultat attendu.
     * </p>
     *
     * <p>
     * L'alogrithme de calcul se veut efficace et sans passer par les exponnentielles.
     * </p>
     *
     * @param x  la base de la puissance
     * @param n  la puissance
     * 
     * @return le résultat de <code>x^n</code>.
     */
    public static int intpower(int x, int n) {
        if (n == 0) return 1;
        if (n == 1) return x;
        int r = intpower(x, n>>1);
        r = r*r;
        if ((n&1) == 0) return r;
        else return r*x;
    }
    
}
