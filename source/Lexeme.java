/*
 * Pour compiler depuis répertoire ./ :
 * javac -d ../bin/ *.java
 *
 * Pour générer la javadoc depuis répertoire ./ :
 * javadoc -private -d ../doc/ -encoding "utf-8" -doencoding "utf-8" -charset "utf-8" *.java
 */

/**
 * Classe représentant un lexème pour l'analyse alphabétique et lexicale.
 *
 * <p>
 * Un objet Lexème représente un quanta d'information pour les analyses alphabétique et lexicale.
 * Chaque objet a un type et une chaîne de caractère. Cette chaîne n'est utile que pour conserver des informations,
 * par exemple pour conserver les valeurs numériques, ou les noms des variables et des fonctions.
 * </p>
 *
 * <p>
 * Les lexèmes qui ont besoin de conserver une information sont de type {@link #INT}, {@link #IDF}, {@link #TYP}, {@link #FCT}
 * et {@link #MAJ}.
 * Pour les autres, la chaîne n'a aucun sens et doit être fixée à <code>null</code>.
 * </p>
 *
 * @see AnalyseCaractere
 * @see AnalyseLexicale
 */
public class Lexeme {
    
    /** Le lexème signale une erreur. */
    public static final int ERR = -1;
    
    /** Le lexème représente le signe point-virgule ';'. Il signale la fin de l'analyse. */
    public static final int END = 0;

    /** Le lexème est un entier. */
    public static final int INT = 1;

    /** Le lexème représente le signe de l'addition '+'. */
    public static final int ADD = 2;

    /** Le lexème représente le signe de la soustraction et de l'opposé '-'. */
    public static final int SUB = 3;

    /** Le lexème représente le signe de la multiplication '*'. */
    public static final int MUL = 4;

    /** Le lexème représente le signe de la division '/'. */
    public static final int DIV = 5;

    /** Le lexème représente le signe de la variable des polynômes 'X'. */
    public static final int VAR = 6;

    /** Le lexème représente le signe de la puissance '^'. */
    public static final int POW = 7;

    /** Le lexème représente la parenthèse ouvrante '('. */
    public static final int PAO = 8;

    /** Le lexème représente la parenthèse fermante ')'. */
    public static final int PAF = 9;

    /** Le lexème représente les symboles d'espace ' ' ou de tabulation '\t'. */
    public static final int ESP = 10;

    /** Le symbole représente un identifiant de variable. */
    public static final int IDF = 11;

    /** Le lexème représente le signe d'affectation '='. */
    public static final int EGA = 12;

    /** Le lexème représente un type de variable. */
    public static final int TYP = 13;

    /** Le lexème représente le signe virgule ','. */
    public static final int VIR = 14;

    /** Le lexème représente une fonction du langage. */
    public static final int FCT = 15;

    /** Le lexème représente des majuscules. */
    public static final int MAJ = 16;



    /** Le type du lexème. */
    private int type;

    /** La valeur du lexème à conserver. */
    private String str;



    /**
     * Création d'un lexème en précisant son type.
     *
     * @param t  le type du lexème
     */
    public Lexeme(int t) {
        this(t,null);
    }


    /**
     * Création d'un lexème en précisant son type et son caractère.
     *
     * @param t  le type du lexème
     * @param n  caractère conserver
     */
    public Lexeme(int t, char n) {
        this(t,String.valueOf(n));
    }


    /**
     * Création d'un lexème en précisant son type et l'information textuelle à garder.
     *
     * @param t  le type du lexème
     * @param s  l'information textuelle qu'il représente
     */
    public Lexeme(int t, String s) {
        type = t;
        str = s;
    }

    
    

    /**
     * Retourne le type du lexème.
     *
     * @return le type du lexème
     */
    public int getType() {
        return type;
    }

    /**
     * Retourne l'information textuelle représenté par le lexème.
     *
     * @return la chaîne représentée par le lexème
     */
    public String getStr() {
        return str;
    }
    

    /**
     * Retourne une chaîne représentant le type du lexème.
     *
     * <p>
     * La fonction signifie get-String-Type.
     * </p>
     *
     * @return une chaîne représentant le type du lexème
     */
    public String getSType() {
        return getSType(type);
    }


    /**
     * Retourne une représentation du lexème sous forme de chaîne de caractères.
     *
     * <p>
     * Utile pour debbug.
     * </p>
     *
     * @return une chaîne représentant le lexème
     */
    public String toString() {
        String txt = "Lexeme: " + getSType();
        if (type == INT || type == IDF || type == TYP || type == MAJ) txt += ": " + str;
        return txt;
    }


    /**
     * Retourne une chaîne représentant un type de lexème.
     *
     * @param type  le type à représenter sous forme de chaîne
     *
     * @return une chaîne de caractère représentant le type de lexème
     */
    public static String getSType(int type) {
        switch(type) {
        case END: return "FIN";
        case INT: return "ENT";
        case ADD: return "+";
        case SUB: return "-";
        case MUL: return "*";
        case DIV: return "/";
        case VAR: return "X";
        case POW: return "^";
        case PAO: return "(";
        case PAF: return ")";
        case ESP: return " ";
        case EGA: return "=";
        case IDF: return "NOM";
        case TYP: return "TYPE";
        case VIR: return ",";
        case FCT: return "FONCTION";
        case MAJ: return "MAJ";
        }
        return "ERR";
    }
    
}
