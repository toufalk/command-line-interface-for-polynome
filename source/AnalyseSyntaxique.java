/**
 * Analyseur syntaxique.
 *
 * <p>
 * Il prend en entrée les lexèmes de l'analyseur lexical et produit un arbre abstrait.
 * La différence entre arbre ayntaxique et arbre abstrait est que ce dernier ne contient pas de noeud inutile :
 * chaque noeud a un sens sémantique.
 * </p>
 *
 * <p>
 * Cet arbre abstrait est construit dans le constructeur de l'objet <code>AnalyseSyntaxique</code> et stocké en mémoire.
 * </p>
 *
 * <p>
 * On a utilisé un automate à pile en créant une méthode par règle :
 * la méthode <code>creationAxiome</code> appelle la méthode <code>creationExpression</code> qui appelle ...
 * L'avantage de cette implémentation, c'est que la pile de l'automate est alors la pile des appels.
 * Elle se gère donc toute seule.
 * </p>
 *
 * <p>
 * L'ensemble des règles est dans le document accompagnant le projet. Pour faire simple, il y a trois familles :
 * les expressions, les déclarations et les affectations.
 * </p>
 *
 * <p>
 * Chaque méthode <code>creation??</code> génère sa partie de l'arbre abstrait.
 * Les symboles syntaxiques (parenthèses, opérations, puissance,...) sont ignorés pour ne garder que l'essentiel :
 * ils n'étaient utiles que pour construire l'arbre abstrait.
 * </p>
 */
class AnalyseSyntaxique {


    /** l'analyseur lexicale qui fournit les lexèmes. */
    private AnalyseLexicale al;

    /** la racine de l'arbre abstrait construit par l'analyseur syntaxique. */
    private ArbreAbstrait souche;



    /**
     * Construction de l'analyseur.
     *
     * <p>
     * Le constructeur construit l'arbre abstrait.
     * </p>
     *
     * @param anal  l'analyseur lexical qui fournit les lexèmes.
     *
     * @throws AnalyseException  si un problème syntaxique survient.
     */
    public AnalyseSyntaxique(AnalyseLexicale anal) throws AnalyseException {
        al = anal;
        souche = creationAxiome();
    }


    
    /**
     * Retourne l'arbre abstrait construit par l'analyseur.
     *
     * @return l'arbre abstrait représentant l'expression.
     */
    public ArbreAbstrait getArbreAbstrait() {
        return souche;
    }

    
    


    /**
     * Règle <code>Axiome &rarr; Expression; | Declaration; | Affectation;</code>.
     *
     * <p>
     * <code>Axiome</code> est l'axiome de la grammaire.<br />
     * <code>Expression</code> représente une expression.<br />
     * <code>Declaration</code> représente une déclaration.<br />
     * <code>Affectation</code> représente une affectation.
     * </p>
     *
     * <p>
     * Pour cette règle, on a besoin d'un passage de la grammaire en LL(2).
     * En effet, il n'est pas possible (de manière simple et cohérente) de
     * différentier une affectation d'une expression qui commence par un
     * identificateur de variable.
     * C'est le seul passage en LL(2).
     * </p>
     *
     * @return l'arbre crée par un Axiome.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationExpression creationExpression
     * @see #creationDeclaration creationDeclaration
     * @see #creationAffectation creationAffectation
     */
    private ArbreAbstrait creationAxiome() throws AnalyseException {
        ArbreAbstrait arbre = null;
        int lextype = al.getNextLexemeType();
        if ((lextype == Lexeme.INT) ||
            (lextype == Lexeme.VAR) ||
            (lextype == Lexeme.FCT) ||
            (lextype == Lexeme.PAO) ||
            (lextype == Lexeme.SUB)) {
            arbre = creationExpression();
        }
        if (lextype == Lexeme.TYP) {
            arbre = creationDeclaration();
        }
        if (lextype == Lexeme.IDF) {
            // passage local en LL(2) pour différentier expression et affectation
            int nextlextype = al.getNextNextLexemeType();
            if (nextlextype == Lexeme.EGA)
                arbre = creationAffectation();
            else
                arbre = creationExpression();
        }
        if (arbre != null) {
            Lexeme lex = al.getNextLexeme();
            if (lex.getType() != Lexeme.END)
                throw new AnalyseException("Lexème " + lex.getSType() + " en trop.");
            return arbre;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.INT) + ", " +
                                   Lexeme.getSType(Lexeme.VAR) + ", " +
                                   Lexeme.getSType(Lexeme.FCT) + ", " +
                                   Lexeme.getSType(Lexeme.PAO) + ", " +
                                   Lexeme.getSType(Lexeme.SUB) + ", " +
                                   Lexeme.getSType(Lexeme.TYP) + ", " +
                                   Lexeme.getSType(Lexeme.IDF));
    }



    /**
     * Règle <code>Expression &rarr; Terme TermeSupp | - Terme TermeSupp</code>.
     *
     * <p>
     * <code>Expression</code> représente une expression.<br />
     * <code>Terme</code> représente un terme d'une addition.<br />
     * <code>TermeSupp</code> représente un éventuel terme supplémentaire.
     * </p>
     *
     * @return l'arbre crée par une expression.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationTerme creationTerme
     * @see #creationTermeSupp creationTermeSupp
     */
    private ArbreAbstrait creationExpression() throws AnalyseException {
        int lextype = al.getNextLexemeType();
        ArbreAbstrait narbre = null;
        if (lextype == Lexeme.SUB) {
            al.getNextLexeme();
            narbre = new ArbreAbstrait(ArbreAbstrait.MINUS);
            lextype = al.getNextLexemeType();
        }
        if ((lextype == Lexeme.INT) ||
            (lextype == Lexeme.IDF) ||
            (lextype == Lexeme.FCT) ||
            (lextype == Lexeme.VAR) ||
            (lextype == Lexeme.PAO)) {
            ArbreAbstrait arbre = creationTerme();
            if (narbre != null) {
                narbre.setAAGauche(arbre);
                arbre = narbre;
            }
            arbre = creationTermeSupp(arbre);
            return arbre;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.SUB) + ", " +
                                   Lexeme.getSType(Lexeme.INT) + ", " +
                                   Lexeme.getSType(Lexeme.IDF) + ", " +
                                   Lexeme.getSType(Lexeme.FCT) + ", " +
                                   Lexeme.getSType(Lexeme.VAR) + ", " +
                                   Lexeme.getSType(Lexeme.PAO));
    }



    /**
     * Règle <code>TermeSupp &rarr; + Terme TermeSupp | - Terme TermeSupp | eps</code>.
     *
     * <p>
     * <code>Terme</code> représente un terme d'une addition.<br />
     * <code>TermeSupp</code> représente un éventuel terme supplémentaire.
     * </p>
     * <p>
     * Cette règle est là s'il y a plus d'une addition/soustraction.
     * </p>
     *
     * @param root  arbre abstrait déjà crée, qui servira de fils gauche.
     *
     * @return l'arbre crée par un terme supplémentaire.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationTerme creationTerme
     */
    private ArbreAbstrait creationTermeSupp(ArbreAbstrait root) throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if ((lextype == Lexeme.ADD) ||
            (lextype == Lexeme.SUB)) {
            int coeff = (lextype == Lexeme.ADD) ? ArbreAbstrait.ADDIT : ArbreAbstrait.SUBST;
            ArbreAbstrait arbre = new ArbreAbstrait(coeff);
            al.getNextLexeme();
            arbre.setAAGauche(root);
            arbre.setAADroit(creationTerme());
            arbre = creationTermeSupp(arbre);
            return arbre;
        }
        if ((lextype == Lexeme.PAF) ||
            (lextype == Lexeme.VIR) ||
            (lextype == Lexeme.END))
            return root;
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.ADD) + ", " +
                                   Lexeme.getSType(Lexeme.SUB) + ", " +
                                   Lexeme.getSType(Lexeme.PAF) + ", " +
                                   Lexeme.getSType(Lexeme.VIR) + ", " +
                                   Lexeme.getSType(Lexeme.END));
    }



    /**
     * Règle <code>Terme &rarr; Facteur FacteurSupp</code>.
     *
     * <p>
     * <code>Terme</code> représente un terme d'une addition.<br />
     * <code>Facteur</code> représente un facteur d'une multiplication.<br />
     * <code>FacteurSupp</code> représente un éventuel facteur supplémentaire.
     * </p>
     *
     * @return l'arbre crée par un terme.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationFacteur creationFacteur
     * @see #creationFacteurSupp creationFacteurSupp
     */
    private ArbreAbstrait creationTerme() throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if ((lextype == Lexeme.INT) ||
            (lextype == Lexeme.IDF) ||
            (lextype == Lexeme.FCT) ||
            (lextype == Lexeme.VAR) ||
            (lextype == Lexeme.PAO)) {
            ArbreAbstrait arbre = creationFacteur();
            arbre  = creationFacteurSupp(arbre);
            return arbre;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.INT) + ", " +
                                   Lexeme.getSType(Lexeme.IDF) + ", " +
                                   Lexeme.getSType(Lexeme.FCT) + ", " +
                                   Lexeme.getSType(Lexeme.VAR) + ", " +
                                   Lexeme.getSType(Lexeme.PAO));
    }



    /**
     * Règle <code>FacteurSupp &rarr; * Facteur FacteurSupp | eps</code>.
     *
     * <p>
     * <code>Facteur</code> représente un facteur d'une multiplication.<br />
     * <code>FacteurSupp</code> représente un éventuel facteur supplémentaire.
     * </p>
     * <p>
     * Cette règle est là s'il y a plus d'une multiplication dans un terme.
     * </p>
     *
     * @param root  arbre abstrait déjà crée, qui servira de fils gauche.
     *
     * @return l'arbre crée par un facteur supplémentaire.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationFacteur creationFacteur
     */
    private ArbreAbstrait creationFacteurSupp(ArbreAbstrait root) throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.MUL) {
            al.getNextLexeme();
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.MULTI);
            arbre.setAAGauche(root);
            arbre.setAADroit(creationFacteur());
            arbre = creationFacteurSupp(arbre);
            return arbre;
        }
        if ((lextype == Lexeme.ADD) ||
            (lextype == Lexeme.SUB) ||
            (lextype == Lexeme.PAF) ||
            (lextype == Lexeme.VIR) ||
            (lextype == Lexeme.END)) {
            return root;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.MUL) + ", " +
                                   Lexeme.getSType(Lexeme.ADD) + ", " +
                                   Lexeme.getSType(Lexeme.SUB) + ", " +
                                   Lexeme.getSType(Lexeme.PAF) + ", " +
                                   Lexeme.getSType(Lexeme.VIR) + ", " +
                                   Lexeme.getSType(Lexeme.END));
    }



    /**
     * Règle <code>Facteur &rarr; Terminal Puissance Denominateur ProduitSupp </code>.
     *
     * <p>
     * <code>Facteur</code> représente un facteur d'une multiplication.<br />
     * <code>Terminal</code> représente un terminal : nombre, identifiant,...<br />
     * <code>Puissance</code> représente une éventuelle puissance.<br />
     * <code>Denominateur</code> représente un éventuelle dénominateur.<br />
     * <code>ProduitSupp</code> représente un éventuel produit supplémentaire.<br />
     * </p>
     *
     * @return l'arbre crée par un facteur.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationTerminal creationTerminal
     * @see #creationPuissance creationPuissance
     * @see #creationDenominateur creationDenominateur
     * @see #creationProduitSupp creationProduitSupp
     */
    private ArbreAbstrait creationFacteur() throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if ((lextype == Lexeme.INT) ||
            (lextype == Lexeme.IDF) ||
            (lextype == Lexeme.FCT) ||
            (lextype == Lexeme.VAR) ||
            (lextype == Lexeme.PAO)) {
            ArbreAbstrait arbre = creationTerminal();
            arbre = creationPuissance(arbre);
            arbre = creationDenominateur(arbre);
            arbre = creationProduitSupp(arbre);
            return arbre;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.INT) + ", " +
                                   Lexeme.getSType(Lexeme.IDF) + ", " +
                                   Lexeme.getSType(Lexeme.FCT) + ", " +
                                   Lexeme.getSType(Lexeme.VAR) + ", " +
                                   Lexeme.getSType(Lexeme.PAO));
    }
    
    
    /**
     * Règle <code>ProduitSupp &rarr; Terminal Puissance Denominateur ProduitSupp | eps</code>.
     * 
     * <p>
     * <code>Terminal</code> représente un terminal : nombre, identifiant,...<br />
     * <code>Puissance</code> représente une éventuelle puissance.<br />
     * <code>Denominateur</code> représente un éventuel dénominateur.
     * </p>
     * 
     * <p>
     * Un produit se distengue d'un facteur par l'absence du signe de la multiplication :
     * <code>4*X</code> sont deux facteurs, <code>4X</code> sont deux produits.
     * </p>
     *
     * @param root  arbre abstrait déjà crée, qui servira de fils gauche.
     *
     * @return l'arbre crée par un produit supplémentaire.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationTerminal creationTerminal
     * @see #creationPuissance creationPuissance
     * @see #creationDenominateur creationDenominateur
    */
    private ArbreAbstrait creationProduitSupp(ArbreAbstrait root) throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if ((lextype == Lexeme.INT) ||
            (lextype == Lexeme.IDF) ||
            (lextype == Lexeme.FCT) ||
            (lextype == Lexeme.VAR) ||
            (lextype == Lexeme.PAO)) {
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.MULTI);
            arbre.setAAGauche(root);
            ArbreAbstrait narbre = creationTerminal();
            narbre = creationPuissance(narbre);
            arbre.setAADroit(narbre);
            arbre = creationDenominateur(arbre);
            arbre = creationProduitSupp(arbre);
            return arbre;
        }
        if ((lextype == Lexeme.MUL) ||
            (lextype == Lexeme.ADD) ||
            (lextype == Lexeme.SUB) ||
            (lextype == Lexeme.PAF) ||
            (lextype == Lexeme.VIR) ||
            (lextype == Lexeme.END)) {
            return root;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.INT) + ", " +
                                   Lexeme.getSType(Lexeme.IDF) + ", " +
                                   Lexeme.getSType(Lexeme.FCT) + ", " +
                                   Lexeme.getSType(Lexeme.VAR) + ", " +
                                   Lexeme.getSType(Lexeme.PAO) + ", " +
                                   Lexeme.getSType(Lexeme.DIV) + ", " +
                                   Lexeme.getSType(Lexeme.MUL) + ", " +
                                   Lexeme.getSType(Lexeme.ADD) + ", " +
                                   Lexeme.getSType(Lexeme.SUB) + ", " +
                                   Lexeme.getSType(Lexeme.PAF) + ", " +
                                   Lexeme.getSType(Lexeme.VIR) + ", " +
                                   Lexeme.getSType(Lexeme.END));
    }
 


    /**
     * Règle <code>Denominateur &rarr; / Terminal Puissance | eps</code>.
     *
     * <p>
     * <code>Terminal</code> représente un terminal : nombre, identifiant,...<br />
     * <code>Puissance</code> représente une éventuelle puissance.
     * </p>
     *
     * @param root  arbre abstrait déjà crée, qui servira de fils gauche.
     *
     * @return l'arbre crée par un dénominateur.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationTerminal creationTerminal
     * @see #creationPuissance creationPuissance
     */
    private ArbreAbstrait creationDenominateur(ArbreAbstrait root) throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.DIV) {
            al.getNextLexeme();
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.DIVID);
            arbre.setAAGauche(root);
            ArbreAbstrait narbre = creationTerminal();
            narbre = creationPuissance(narbre);
            arbre.setAADroit(narbre);
            return arbre;
        }
        if ((lextype == Lexeme.INT) ||
            (lextype == Lexeme.IDF) ||
            (lextype == Lexeme.FCT) ||
            (lextype == Lexeme.VAR) ||
            (lextype == Lexeme.PAO) ||
            (lextype == Lexeme.MUL) ||
            (lextype == Lexeme.ADD) ||
            (lextype == Lexeme.SUB) ||
            (lextype == Lexeme.PAF) ||
            (lextype == Lexeme.VIR) ||
            (lextype == Lexeme.END)) {
            return root;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.DIV) + ", " +
                                   Lexeme.getSType(Lexeme.INT) + ", " +
                                   Lexeme.getSType(Lexeme.IDF) + ", " +
                                   Lexeme.getSType(Lexeme.FCT) + ", " +
                                   Lexeme.getSType(Lexeme.VAR) + ", " +
                                   Lexeme.getSType(Lexeme.PAO) + ", " +
                                   Lexeme.getSType(Lexeme.MUL) + ", " +
                                   Lexeme.getSType(Lexeme.ADD) + ", " +
                                   Lexeme.getSType(Lexeme.SUB) + ", " +
                                   Lexeme.getSType(Lexeme.PAF) + ", " +
                                   Lexeme.getSType(Lexeme.VIR) + ", " +
                                   Lexeme.getSType(Lexeme.END));
    }



    /**
     * Règle <code>Puissance &rarr; ^ Terminal | eps</code>.
     *
     * <p>
     * <code>Terminal</code> représente un terminal : nombre, identifiant,...
     * </p>
     *
     * @param root  arbre abstrait déjà crée, qui servira de fils gauche.
     *
     * @return l'arbre crée par une puissance.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationTerminal creationTerminal
     */
    private ArbreAbstrait creationPuissance(ArbreAbstrait root) throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.POW) {
            al.getNextLexeme();
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.POWER);
            arbre.setAAGauche(root);
            arbre.setAADroit(creationTerminal());
            return arbre;
        }
        if ((lextype == Lexeme.DIV) ||
            (lextype == Lexeme.IDF) ||
            (lextype == Lexeme.INT) ||
            (lextype == Lexeme.FCT) ||
            (lextype == Lexeme.VAR) ||
            (lextype == Lexeme.PAO) ||
            (lextype == Lexeme.MUL) ||
            (lextype == Lexeme.ADD) ||
            (lextype == Lexeme.SUB) ||
            (lextype == Lexeme.PAF) ||
            (lextype == Lexeme.VIR) ||
            (lextype == Lexeme.END)) {
            return root;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.POW) + ", " +
                                   Lexeme.getSType(Lexeme.DIV) + ", " +
                                   Lexeme.getSType(Lexeme.IDF) + ", " +
                                   Lexeme.getSType(Lexeme.INT) + ", " +
                                   Lexeme.getSType(Lexeme.FCT) + ", " +
                                   Lexeme.getSType(Lexeme.VAR) + ", " +
                                   Lexeme.getSType(Lexeme.PAO) + ", " +
                                   Lexeme.getSType(Lexeme.MUL) + ", " +
                                   Lexeme.getSType(Lexeme.ADD) + ", " +
                                   Lexeme.getSType(Lexeme.SUB) + ", " +
                                   Lexeme.getSType(Lexeme.PAF) + ", " +
                                   Lexeme.getSType(Lexeme.VIR) + ", " +
                                   Lexeme.getSType(Lexeme.END));
    }
    
    

    /**
     * Règle <code>Terminal &rarr; Entier | Identificateur | Fonction | Variable | ( Expression )</code>.
     *
     * <p>
     * <code>Entier</code> représente un nombre.<br />
     * <code>Identificateur</code> représente un nom de variable.<br />
     * <code>Fonction</code> représente une fonction du langage des polynômes.<br />
     * <code>Variable</code> représente la variable X.<br />
     * <code>Expression</code> représente une expression polynomiale.<br />
     * </p>
     *
     * <p>
     * Ce n'est pas à proprement parlé un terminal (à cause des fonctions et des expressions) mais, c'est c'est tout comme...
     * </p>
     *
     * @return l'arbre crée par un terminal.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationEntier creationEntier
     * @see #creationIdentificateur creationIdentificateur
     * @see #creationFonction creationFonction
     * @see #creationVariable creationVariable
     * @see #creationExpression creationExpression
     */
    private ArbreAbstrait creationTerminal() throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.INT) return creationEntier();
        if (lextype == Lexeme.IDF) return creationIdentificateur(ArbreAbstrait.IDVAL);
        if (lextype == Lexeme.FCT) return creationFonction();
        if (lextype == Lexeme.VAR) return creationVariable();
        if (lextype == Lexeme.PAO) {
            al.getNextLexeme();
            ArbreAbstrait arbre = creationExpression();
            lextype = al.getNextLexemeType();
            if (lextype == Lexeme.PAF) {
                al.getNextLexeme();
                return arbre;
            }
            throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                       " impévu. Attendu: " +
                                       Lexeme.getSType(Lexeme.PAF));
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.INT) + ", " +
                                   Lexeme.getSType(Lexeme.IDF) + ", " +
                                   Lexeme.getSType(Lexeme.FCT) + ", " +
                                   Lexeme.getSType(Lexeme.VAR) + ", " +
                                   Lexeme.getSType(Lexeme.PAO));
    }
    


    /**
     * Règle <code>Entier &rarr; entier</code>.
     *
     * @return l'arbre crée par un entier.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     */
    private ArbreAbstrait creationEntier() throws AnalyseException {
        Lexeme lex = al.getNextLexeme();
        if (lex.getType() == Lexeme.INT) {
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.LEAFN);
            arbre.setSLeaf(lex.getStr());
            return arbre;
        }
        throw new AnalyseException("Lexème " + lex.getSType() +
                                   " imprévu. Attendu: " +
                                   Lexeme.getSType(Lexeme.INT));
    }


    /**
     * Règle <code>Variable &rarr; 'X'</code>.
     *
     * @return l'arbre crée par la variable du polynôme.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     */
    private ArbreAbstrait creationVariable() throws AnalyseException {
        Lexeme lex = al.getNextLexeme();
        if (lex.getType() == Lexeme.VAR) {
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.LEAFX);
            return arbre;
        }
        throw new AnalyseException("Lexème " + lex.getSType() +
                                   " imprévu. Attendu: " +
                                   Lexeme.getSType(Lexeme.VAR));

    }



    /**
     * Règle <code>Declaration &rarr; Typage Identificateur AffectEvent DeclaSupp</code>.
     *
     * <p>
     * <code>Typage</code> représente un type de variable.<br />
     * <code>Identificateur</code> représente un identifiant de variable.<br />
     * <code>AffectEvent</code> représente une éventuelle affectation après déclaration.<br />
     * <code>DeclaSupp</code> représente une éventuelle déclaration supplémentaire.
     * </p>
     *
     * @return l'arbre crée par une déclaration.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationTypage creationTypage
     * @see #creationIdentificateur creationIdentificateur
     * @see #creationAffectEvent creationAffectEvent
     * @see #creationDeclaSupp creationDeclaSupp
     */
    private ArbreAbstrait creationDeclaration() throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.TYP) {
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.DECLA);
            arbre.setAAGauche(creationTypage());
            arbre.setAADroit(creationIdentificateur(ArbreAbstrait.IDAFF));
            arbre = creationAffectEvent(arbre);
            arbre = creationDeclaSupp(arbre);
            return arbre;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendu: " +
                                   Lexeme.getSType(Lexeme.TYP));
    }
    


    /**
     * Règle <code>Typage &rarr; type</code>.
     *
     * <p>
     * Les types acceptés sont <code>INT</code>, <code>FRAC</code> ou <code>POLY</code>.
     * </p>
     *
     * @return l'arbre crée par un typage.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse.
     */
    private ArbreAbstrait creationTypage() throws AnalyseException {
        Lexeme lex = al.getNextLexeme();
        if (lex.getType() == Lexeme.TYP) {
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.TYPAG);
            arbre.setSLeaf(lex.getStr());
            return arbre;
        }
        throw new AnalyseException("Lexème " + lex.getSType() +
                                   " imprévu. Attendu: " +
                                   Lexeme.getSType(Lexeme.TYP));
    }


    
    /**
     * Règle <code>Identificateur &rarr; identifiant de variable</code>.
     *
     * <p>
     * Le paramètre <code>type</code> de la méthode précise l'utilisation
     * de l'identifiant pour spécifier le noeud de l'arbre.
     * Les types envisagés sont <code>ArbreAbstrait.IDAFF</code> pour une affectation
     * ou <code>ArbreAbstrait.IDVAL</code> pour une utilisation de la variable.
     * </p>
     *
     * @param type  type de noeud souhaité.
     *
     * @return l'arbre crée par un identificateur.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     */
    private ArbreAbstrait creationIdentificateur(int type) throws AnalyseException {
        Lexeme lex = al.getNextLexeme();
        if (lex.getType() == Lexeme.IDF) {
            ArbreAbstrait arbre = new ArbreAbstrait(type);
            arbre.setSLeaf(lex.getStr());
            return arbre;
        }
        throw new AnalyseException("Lexème " + lex.getSType() +
                                   " imprévu. Attendu: " +
                                   Lexeme.getSType(Lexeme.IDF));
    }




    /**
     * Règle <code>DeclaSupp &rarr; , Identificateur AffectEvent DeclaSupp | eps</code>.
     *
     * <p>
     * <code>Identificateur</code> représente un identifiant de variable.<br />
     * <code>AffectEvent</code> représente une éventuelle affectation.<br />
     * </p>
     *
     * @param root  arbre abstrait déjà crée, qui servira de fils gauche.
     *
     * @return l'arbre crée par une déclaration supplémentaire.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationIdentificateur creationIdentificateur
     * @see #creationAffectEvent creationAffectEvent
     */
    private ArbreAbstrait creationDeclaSupp(ArbreAbstrait root) throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.VIR) {
            al.getNextLexeme();
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.DECLA);
            arbre.setAAGauche(root);
            arbre.setAADroit(creationIdentificateur(ArbreAbstrait.IDAFF));
            arbre = creationAffectEvent(arbre);
            arbre = creationDeclaSupp(arbre);
            return arbre;
        }
        if (lextype == Lexeme.END) {
            return root;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.VIR) + ", " +
                                   Lexeme.getSType(Lexeme.END));
    }




    /**
     * Règle <code>AffectEvent &rarr; = Expression | eps</code>.
     *
     * <p>
     * <code>Expression</code> représente une expression.
     * </p>
     *
     * @param root  arbre abstrait déjà crée, qui servira de fils gauche.
     *
     * @return l'arbre crée par une affectation juste après la déclaration.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationExpression creationExpression
     */
    private ArbreAbstrait creationAffectEvent(ArbreAbstrait root) throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.EGA) {
            al.getNextLexeme();
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.AFFEC);
            arbre.setAAGauche(root);
            arbre.setAADroit(creationExpression());
            return arbre;
        }
        if ((lextype == Lexeme.VIR) ||
            (lextype == Lexeme.END)) {
            return root;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendus: " +
                                   Lexeme.getSType(Lexeme.EGA) + ", " +
                                   Lexeme.getSType(Lexeme.VIR) + ", " +
                                   Lexeme.getSType(Lexeme.END));
    }

    

    /**
     * Règle <code>Affectation &rarr; Identificateur = Expression</code>.
     *
     * <p>
     * <code>Identificateur</code> représente un identifiant de variable.<br />
     * <code>Expression</code> représente une expression.
     * </p>
     *
     * @return l'arbre crée par une affectation.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationIdentificateur creationIdentificateur
     * @see #creationExpression creationExpression
     */
    private ArbreAbstrait creationAffectation() throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.IDF) {
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.AFFEC);
            arbre.setAAGauche(creationIdentificateur(ArbreAbstrait.IDAFF));
            Lexeme lex = al.getNextLexeme();
            if (lex.getType() != Lexeme.EGA)
                throw new AnalyseException("Lexème " + lex.getSType() +
                                           " imprévu. Attendu: " +
                                           Lexeme.getSType(Lexeme.EGA));
            arbre.setAADroit(creationExpression());
            return arbre;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   " imprévu. Attendu: " +
                                   Lexeme.getSType(Lexeme.IDF));
    }



    /**
     * Règle <code>Fonction &rarr; fonction(Expression,...)</code>.
     *
     * <p>
     * <code>Expression</code> représente une expression.
     * </p>
     *
     * @return l'arbre crée par une fonction du langage des polynôme.
     *
     * @throws AnalyseException  si un problème survient lors de l'analyse
     *
     * @see #creationExpression creationExpression
     */
    private ArbreAbstrait creationFonction() throws AnalyseException {
        int lextype = al.getNextLexemeType();
        if (lextype == Lexeme.FCT) {
            ArbreAbstrait arbre = new ArbreAbstrait(ArbreAbstrait.FONCT);
            Lexeme lex = al.getNextLexeme();
            arbre.setSLeaf(lex.getStr());
            lex = al.getNextLexeme();
            if (lex.getType() != Lexeme.PAO)
                throw new AnalyseException("Parenthèse ouvrante attendue après la fonction " +
                                           arbre.getSLeaf());
            arbre.setAAGauche(creationExpression());
            // fonctions à 2 variables
            if ("QUO".equals(arbre.getSLeaf()) || "REM".equals(arbre.getSLeaf())) {
                lex = al.getNextLexeme();
                if (lex.getType() != Lexeme.VIR)
                    throw new AnalyseException("Virgule attendue entre les paramètres de la fonction " +
                                               arbre.getSLeaf());
                arbre.setAADroit(creationExpression());
            }
            lex = al.getNextLexeme();
            if (lex.getType() != Lexeme.PAF)
                throw new AnalyseException("Parenthèse fermante attendue après la fonction " +
                                           arbre.getSLeaf());
            return arbre;
        }
        throw new AnalyseException("Lexème " + Lexeme.getSType(lextype) +
                                   "imprévu. Attendu: " +
                                   Lexeme.getSType(Lexeme.FCT));
    }

    
}
