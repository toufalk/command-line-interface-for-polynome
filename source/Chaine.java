/**
 * Il s'agit d'une implémentation d'une liste doublement chaînée.
 * Les contenus des maillons sont des monômes.
 */
class Chaine {


    /** Contenu du maillon. */
    private Monome content;

    /** maillon suivant. */
    private Chaine next;

    /** maillon précédent. */
    private Chaine previous;
    
    
    
    /**
     * Construction d'un maillon.
     *
     * <p>
     * Les suivants et précédents sont <code>null</code>.
     * </p>
     *
     * @param mon le monôme contenu dans le maillon.
     */
    public Chaine(Monome mon) {
        this(null, mon, null);
    }


    /**
     * Construction d'un maillon.
     *
     * @param avant maillon précédent.
     * @param mon le monôme contenu dans le maillon.
     * @param apres maillon suivant.
     *
     */
    public Chaine(Chaine avant, Monome mon, Chaine apres) {
        content = mon;
        concatene(avant,this);
        concatene(this,apres);
    }
    


    /**
     * Retourne le monôme contenu dans le maillon.
     *
     * @return le contenu du maillon.
     */
    public Monome getMonome() {
        return content;
    }


    /**
     * Retourne le maillon suivant.
     *
     * @return le maillon suivant.
     */
    public Chaine getNext() {
        return next;
    }


    /**
     * Retourne le maillon précédent.
     *
     * @return le maillon précédent.
     */
    public Chaine getPrevious() {
        return previous;
    }


    /**
     * Fixe une nouvelle valeur au maillon suivant.
     *
     * @param m le nouveau maillon suivant.
     */
    public void setNext(Chaine m) {
        next = m;
    }


    /**
     * Fixe une nouvelle valeur au maillon précédent.
     *
     * @param m le nouveau maillon précédent.
     */
    public void setPrevious(Chaine m) {
        previous = m;
    }
 


    /**
     * Relie les deux maillons.
     *
     * @param c1  le premier maillon
     * @param c2  le deuxième maillon
     */
    public static void concatene(Chaine c1, Chaine c2) {
        if (c1 != null) c1.setNext(c2);
        if (c2 != null) c2.setPrevious(c1);
    }
    
}
