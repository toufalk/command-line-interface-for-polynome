/**
 * Arbre abstrait des polynômes.
 *
 * <p>
 * Il est crée par l'analyseur syntaxique et utilisé par l'analyseur sémantique.
 * </p>
 *
 * <p>
 * Chaque noeud possède une étiquette sémantique décrivant son action.
 * Chaque feuille possède une chaîne de caractères décrivant son information.
 * </p>
 */
class ArbreAbstrait {

    /** noeud représentant une addition. */
    public static final int ADDIT = 0;

    /** noeud représentant une soustraction. */
    public static final int SUBST = 1;
    
    /** noeud représentant une multiplication. */
    public static final int MULTI = 2;

    /** noeud représentant une division. */
    public static final int DIVID = 3;

    /** noeud représentant une puissance. */
    public static final int POWER = 4;

    /** noeud représentant un moins unaire. */
    public static final int MINUS = 5;

    /** feuille représentant un nombre. */
    public static final int LEAFN = 6;
    
    /** feuille représentant la variable X des polynômes. */
    public static final int LEAFX = 7;

    /** feuille représentant un type de variable. */
    public static final int TYPAG = 8;

    /** feuille représentant un identifiant en vue d'une affectation (à gauche du =). */
    public static final int IDAFF = 9;

    /** feuille représentant un identifiant en vue de connaître sa valeur (à droite du = ou dans une expression). */
    public static final int IDVAL = 10;

    /** noeud représentant une déclaration d'identifiant. */
    public static final int DECLA = 11;

    /** noeud représentant une affectation. */
    public static final int AFFEC = 12;

    /** feuille représentant une fonction du langage. */
    public static final int FONCT = 13;
    


    /** l'étiquette du noeud. */
    private int label;

    /** le fils gauche du noeud. */
    private ArbreAbstrait aagauche;

    /** le fils droit du noeud. */
    private ArbreAbstrait aadroit;

    /** une chaîne représentant l'information de la feuille. */
    private String leaf;



    /**
     * Création d'un arbre abstrait défini par son type.
     *
     * @param t  type de l'arbre crée.
     */
    public ArbreAbstrait(int t) {
        label = t;
        aagauche = null;
        aadroit = null;
        leaf = null;
    }


    /**
     * Définie le fils gauche de ce noeud.
     *
     * @param aa  le fils gauche.
     */
    public void setAAGauche(ArbreAbstrait aa) {
        aagauche = aa;
    }

    /**
     * Définie le fils droit de ce noeud.
     *
     * @param aa  le fils droit.
     */
    public void setAADroit(ArbreAbstrait aa) {
        aadroit = aa;
    }


    /**
     * Définie la chaîne représentant l'information de la feuille.
     *
     * @param str  la chaîne de la feuille.
     */
    public void setSLeaf(String str) {
        leaf = str;
    }



    /**
     * Retourne le type (l'étiquette) du noeud.
     *
     * @return le type du noeud.
     */
    public int getLabel() {
        return label;
    }


    /**
     * Retourne une représentation de l'étiquette du noeud sous forme de chaîne de caractères.
     *
     * @return une chaîne représentant le type du noeud.
     */
    public String getSLabel() {
        switch(label) {
        case ADDIT: return "{+}";
        case SUBST: return "{-}";
        case MULTI: return "{*}";
        case DIVID: return "{/}";
        case POWER: return "{^}";
        case MINUS: return "{-}";
        case LEAFN: return "{N:" + leaf + "}";
        case LEAFX: return "{X}";
        case TYPAG: return "{T:" + leaf + "}";
        case IDVAL: return "{V:" + leaf + "}";
        case IDAFF: return "{A:" + leaf + "}";
        case DECLA: return "{:}";
        case AFFEC: return "{=}";
        case FONCT: return "{F:" + leaf + "}";
        }
        return "Erreur";
    }


    /**
     * Retourne le sous-arbre gauche du noeud.
     *
     * @return le sous arbre gauche.
     */
    public ArbreAbstrait getAAGauche() {
        return aagauche;
    }


    /**
     * Retourne le sous-arbre droit du noeud.
     *
     * @return le sous arbre droit.
     */
    public ArbreAbstrait getAADroit() {
        return aadroit;
    }


    /**
     * Retourne la chaîne de caractère d'information de la feuille.
     *
     * @return la chaîne de caractère de la feuille.
     */
    public String getSLeaf() {
        return leaf;
    }
    


    /**
     * Retourne une représentation de l'arbre sous forme de chaîne de caractères.
     *
     * <p>
     * Cette méthode est une méthode générique. Elle est appelée automatiquement par java
     * dès que l'on fait <code>System.out.println(arbre);</code>. Elle est très utile, notamment pour debugger.
     * </p>
     *
     * @return une chaîne de caractère représentant l'arbre.
     */
    public String toString() {
        return toString("", false, false);
    }


    /**
     * Retourne une chaîne de caractères représentant l'arbre.
     *
     * <p>
     * Cette méthode est appelée par {@link #toString()}. Elle permet d'afficher l'arbre récurssivement.
     * </p>
     *
     * @param prefix  chaîne à écrire avant les infos du noeud.
     * @param affGauche  doit-on afficher le symbole '<code>|</code>' symbolisant la branche de gauche ?
     * @param affDroit  doit-on afficher le symbole '<code>|</code>' symbolisant la branche de droite ?
     *
     * @return une chaîne de caractères représentant l'arbre.
     */
    public String toString(String prefix, boolean affGauche, boolean affDroit) {
        String pga, pdr, msg;
        if (affGauche) pga = "|    ";
        else pga = "     ";
        if (affDroit) pdr = "|    ";
        else pdr = "     ";
        msg = "";
        if (aagauche != null) msg += aagauche.toString(prefix+pga, false, true);
        msg += prefix + "+---" + getSLabel() + "\n";
        if (aadroit != null) msg += aadroit.toString(prefix+pdr,true, false);
        return msg;
    }

    
}
