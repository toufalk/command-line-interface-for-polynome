import java.util.Hashtable;

/**
 * Mémoire de travail des polynômes.
 *
 * <p>
 * Cette mémoire stocke les associations nom-valeur des valiables.
 * Ceci permet de savoir l'état d'une variable.
 * </p>
 *
 * <p>
 * La mémoire est utilisée par l'analyseur sémantique dynamique.
 * </p>
 *
 * <p>
 * Cette classe n'a pas de constructeur publique. Il n'est donc pas possible de créer des objets
 * <code>Memoire</code>. On doit nécessairement passer par son instance statique <code>mem</code>.
 * L'idée est que si on a besoin d'une nouvelle mémoire, elle doit se poser sur la pile de mémoires
 * et la gestion de cette pile doit se faire en interne au sein de la classe <code>Memoire</code>.
 * </p>
 *
 * <p>
 * Pour l'instant, il n'est pas possible d'empiler des mémoires car il n'existe pas de notion de blocs
 * pour les polynômes. C'est prévu. Dans ce cas, il existera une méthode statique pour créer une nouvelle
 * mémoire, celle-ci gérera l'empilement et le dépilement.
 * La porter des variables sera, quant à elle, gérée par la pile des mémoires... Wait'n see !
 * </p>
 *
 * @see AnalyseSemantique
 */
class Memoire {


    /** La mémoire en cours. */
    public static Memoire mem = new Memoire();


    /** la table de hachage contenant les associations nom-variable. */
    private Hashtable<String, Variable> table;



    /**
     * Création d'une nouvelle mémoire.
     */
    private Memoire() {
        table = new Hashtable<String, Variable>();
    }
    
    

    /**
     * Ajoute une association entre un nom et une valeur.
     *
     * <p>
     * Si le nom est déjà utilisé, l'ancienne association est remplacée par celle-ci.
     * </p>
     *
     * @param name  identifiant de la viariable.
     * @param val   valeur de la variable.
     */
    public void setValeur(String name, Variable val) {
        table.put(name, val);
    }

    
    /**
     * Donne la valeur associée à un nom.
     *
     * @param name  identifiant de la variable.
     *
     * @return la valeur de la variable <code>name</code>.
     */
    public Variable getValeur(String name) {
        return table.get(name);
    }


    /**
     * Donne l'existence d'une association avec un nom de variable.
     *
     * @param name  identifiant de la variable.
     *
     * @return  <code>true</code> si la variable <code>name</code> est déjà associée à une valeur, <code>false</code> sinon.
     */
    public boolean hasValeur(String name) {
        return table.containsKey(name);
    }
}
