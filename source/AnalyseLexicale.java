/**
 * Classe d'analyse lexicale.
 *
 * <p>
 * Son rôle est de concaténer les lexèmes (lus depuis un analyseur alphabétique) de même type ensemble :
 * les chiffres avec les chiffres, les lettres d'identifiant ou majuscules ensemble.
 * Elle supprime aussi les caractères non significatifs comme les espaces et les tabulations.
 * </p>
 *
 * <p>
 * Elle est basé sur un automate déterministe à quatre états :
 * <ul>
 * <li> un état <code>CARACT</code> pour les caractères non-spéciaux</li>
 * <li> un état <code>NOMBRE</code> pour les caractères numériques</li>
 * <li> un état <code>NOMVAR</code> pour les caractères d'identifiant de variable</li>
 * <li> un état <code>MAJCAR</code> pour les caractères en majuscule</li>
 * </ul>
 * Une fois le lexème lexical formé, on vérifie qu'il est bien conforme aux prescriptions du langage.
 * Un lexème de type <code>MAJ</code> est spécialisé en fonction de sa signification lexicale.
 * </p>
 */
public class AnalyseLexicale {


    /** état de l'automate représentant un caractère non-spécial. */
    private static final int CARACT = 1;

    /** état de l'automate représentant un nombre. */
    private static final int NOMBRE = 2;

    /** état de l'automate représentant un identifiant de variable. */
    private static final int NOMVAR = 3;

    /** état de l'automate représentant des majuscules. */
    private static final int MAJCAR = 4;



    /** l'analyseur alphabétique qui fournit les lexèmes à analyser. */
    private AnalyseCaractere flin;

    /** le tampon pour stocker les chaînes de caractères en cours de construction. */
    private StringBuffer buffer;

    /** l'état actuel de l'automate lexical. */
    private int etat;

    /** le prochain lexème qui sera consommé. */
    private Lexeme next;

    /**
     * le suivant de prochain lexème qui sera consommé.
     * Il nous le faut car l'analyseur syntaxique est en fait LL(2) :
     * il a besoin du type des deux prochains lexèmes pour fonctionner.
     */
    private Lexeme nextnext;


    
    /**
     * Initialise l'analyseur lexical.
     *
     * @param ac  l'analyseur alphabetique qui fournit les lexèmes à analyser
     */
    public AnalyseLexicale(AnalyseCaractere ac) {
        flin = ac;
        buffer = new StringBuffer();
        etat = CARACT;
        next = null;
        nextnext = null;
    }

    

    /**
     * Retourne le type du prochain lexème.
     *
     * <p>
     * Ce lexème n'est pas consommé.
     * </p>
     *
     * @return le type du prochain lexème
     *
     * @throws AnalyseException  si l'analyseur alphabétique a levé une exception ou
     *         si un lexème n'est pas lexicalement bien formé
     *         (voir {@link #valideLexeme(Lexeme) valideLexeme})
     */
    public int getNextLexemeType() throws AnalyseException {
        if (next == null)
            next = getNextLexeme();
        if (next == null)
            return Lexeme.ERR;
        return next.getType();
    }


    /**
     * Retourne le type du lexème suivant le prochain.
     *
     * <p> Aucun lexème n'est consommé avec l'appel à cette méthode. </p>
     *
     * @return le type du suivant du prochaine lexème
     *
     * @throws AnalyseException  si l'analyseur alphabétique a levé une exception ou
     *         si un lexème n'est pas lexicalement bien formé
     *         (voir {@link #valideLexeme(Lexeme) valideLexeme})
     */
    public int getNextNextLexemeType() throws AnalyseException {
        if (nextnext != null) return nextnext.getType();
        Lexeme lex = next;
        next = null;
        if (lex == null) lex = getNextLexeme();
        if (lex == null) return Lexeme.ERR;
        nextnext = getNextLexeme();
        next = lex;
        if (nextnext == null) return Lexeme.ERR;
        return nextnext.getType();
    }


    /**
     * Retourne le prochain lexème.
     *
     * <p>
     * Cette méthode consomme le lexème renvoyé.
     * C'est le coeur de l'automate lexical.
     * </p>
     *
     * @return le prochain lexème
     *
     * @throws AnalyseException  si l'analyseur alphabétique a levé une exception ou
     *         si un lexème n'est pas lexicalement bien formé
     *         (voir {@link #valideLexeme(Lexeme) valideLexeme})
     */
    public Lexeme getNextLexeme() throws AnalyseException {
        Lexeme lex = null;
        if (next != null) {
            lex = next;
            next = nextnext;
            nextnext = null;
            return lex;
        }
        int lextype = flin.getNextLexemeType();
        if (lextype == Lexeme.ERR) return null;
        switch(etat) {
            
        case CARACT: // On était sur un caractère ordinaire.
            lex = flin.getNextLexeme();
            switch(lextype) {
            case Lexeme.INT:
                buffer.append(lex.getStr());
                etat = NOMBRE;
                break;
            case Lexeme.IDF:
                buffer.append(lex.getStr());
                etat = NOMVAR;
                break;
            case Lexeme.MAJ:
                buffer.append(lex.getStr());
                etat = MAJCAR;
                break;
            default:
                if (lextype != Lexeme.ESP) return lex;
            }
            break;
            
        case NOMBRE: // On était sur un caractère numérique.
            if (lextype == Lexeme.INT) {
                buffer.append(flin.getNextLexeme().getStr());
            } else {
                lex = new Lexeme(Lexeme.INT, buffer.toString());
                buffer = new StringBuffer();
                etat = CARACT;
                return valideLexeme(lex);
            }
            break;
            
        case NOMVAR: // On était sur un caractère d'identifiant de variable.
            if (lextype == Lexeme.IDF) {
                buffer.append(flin.getNextLexeme().getStr());
            } else {
                lex = new Lexeme(Lexeme.IDF, buffer.toString());
                buffer = new StringBuffer();
                etat = CARACT;
                return lex;
            }
            break;

        case MAJCAR: // On était sur un caractère majuscule.
            if (lextype == Lexeme.MAJ) {
                buffer.append(flin.getNextLexeme().getStr());
            } else {
                lex = new Lexeme(Lexeme.MAJ, buffer.toString());
                buffer = new StringBuffer();
                etat = CARACT;
                return valideLexeme(lex);
            }
            break;
        }
        /*
          Si on est arrivé là, c'est que
           - on est en train de concaténer des lexèmes alphabétiques de même type entre eux.
           - on est tombé sur un lexème sans signification (un espace)
           On doit donc aller un cran plus loin.
        */
        return getNextLexeme();
    }




    /**
     * Valide lexicalement le lexème.
     *
     * <p>
     * Cette méthode vérifie qu'un nombre non nul ne commence pas par zéro
     * et sélectionne le bon type d'un lexème majuscule.
     * En cas de problème, elle lève une <code>AnalyseException</code>.
     * </p>
     *
     * @param lex  le lexème à valider
     *
     * @return un lexème lexicalement valide
     *
     * @throws AnalyseException  si le lexème n'est pas lexicalement valide
     */
    private Lexeme valideLexeme(Lexeme lex) throws AnalyseException {
        if (lex == null) return lex;
        if (lex.getType() == Lexeme.INT) {
            String nbStr = lex.getStr();
            if (nbStr.length() > 1 && nbStr.charAt(0) == '0')
                throw new AnalyseException("Nombre commencant par zéro.");
        }
        if (lex.getType() == Lexeme.MAJ) {
            String str = lex.getStr();
            if ("ANS".equals(str)) 
                return new Lexeme(Lexeme.IDF, str);
            if ("DEG".equals(str) || "QUO".equals(str) || "REM".equals(str))
                return new Lexeme(Lexeme.FCT, str);
            if ("INT".equals(str) || "FRAC".equals(str) || "POLY".equals(str))
                return new Lexeme(Lexeme.TYP, str);
            throw new AnalyseException("Mot clé " + str + " inconnu.");
        }
        return lex;
    }
    
}
