/**
 * Classe représentant un monôme.
 *
 * <p>
 * Il s'agit de monôme rationnel, c'est-à-dire à coefficient rationnel.
 * Il est définie par son numérateur, son dénominateur et sa puissance.
 * </p>
 *
 * <p>
 * Ils sont à la base des polynômes.
 * </p>
 *
 * <p>
 * Pour le coefficient, on fait appel à un objet {@link Fraction} qui regarde si la fraction a un sens
 * et la simplifie si besoin. On vérifie aussi que la puissance est bien un entier positif ou nul.
 * </p>
 *
 * <p>
 * Les objets <code>Monome</code> sont des objets immutables : une fois définis, ils ne peuvent être modifiés.
 * Toutes les opérations mathématiques dessus (addition, multiplication, puissance,...)
 * devront donc créer de nouveaux objets.
 * </p>
 *
 * <p>
 * Par ailleurs, la classe <code>Monome</code> possède deux monômes statiques qui sont utiles régulièrement :
 * le monôme <code>1</code> et le monôme <code>X</code>.
 * </p
 * 
 * @see Fraction
 * @see Polynome
 */
public class Monome {

    /** Le monôme égale à 1. */
    public static Monome UN;

    /** Le monôme égal à X. */
    public static Monome X;

    // initialisation des monômes statiques
    static {
        try {
            UN = new Monome(1);
            X = new Monome(1,1,1);
        } catch(AnalyseException ae) {}
    }
    

    
    /** le coefficient du monôme. */
    private Fraction frac;

    /** la puissance du monôme. */
    private int pow;



    /**
     * Création d'un monôme entier constant.
     *
     * @param numerateur  le coefficient entier du monôme.
     */
    public Monome(int numerateur) {
        try {
            frac = new Fraction(numerateur, 1);
        } catch (AnalyseException ae) {}
        pow = 0;
    }

    /**
     * Création d'un monôme rationnel constant.
     *
     * @param frac  le coefficient rationnel du monôme.
     */
    public Monome(Fraction frac) {
        this.frac = frac;
        pow = 0;
    }


    /**
     * Création d'un monôme.
     *
     * @param numerateur  le numératuer du monôme.
     * @param denominateur  le dénominateur du monôme.
     * @param puissance  la puissance du monôme.
     *
     * @throws AnalyseException  si le dénominateur est nul ou la puissance négative.
     */
    public Monome(int numerateur, int denominateur, int puissance) throws AnalyseException {
        this(new Fraction(numerateur, denominateur), puissance);
    }


    /**
     *
     *
     * @param frac  le coefficient rationnel du monôme.
     * @param puissance  la puissance du monôme.
     *
     * @throws AnalyseException  si la puissance est négative.
     */
    public Monome(Fraction frac, int puissance) throws AnalyseException {
        this.frac = frac;
        this.pow = puissance;
        if (pow < 0)
            throw new AnalyseException("Puissance négative.");
    }
    
    
    
    
    /**
     * Retourne le numérateur du monôme.
     *
     * @return le numérateur du monôme.
     */
    public int getNumerateur() {
        return frac.getNumerateur();
    }


    /**
     * Retourne le dénominateur du monôme.
     *
     * @return le dénominateur du monôme.
     */
    public int getDenominateur() {
        return frac.getDenominateur();
    }


    /**
     * Retourne la puissance du monôme.
     *
     * @return la puissance du monôme.
     */
    public int getPuissance() {
        return pow;
    }


    /**
     * test si le monôme est nul.
     *
     * @return <code>true</code> si le monôme est nul, <code>false</code> sinon.
     */
    public boolean estNul() {
        return frac.estNulle();
    }
    
    

    /**
     * Retourne une représentation du monôme sous forme de chaîne de caractère.
     *
     * <p>
     * Cette représentation respecte les standards mathématiques : le signe moins en premier,
     * pas de numérateur si la fraction est égale à 1, pas de dénominateur s'il est égal à 1, 
     * pas de <code>X^p</code> si <code>p</code> est égal à 0, pas de puissance si <code>p</code> est égal à 1.
     * </p>
     *
     * @return une représentation du monôme sous forme de chaîne de caractères.
     */
    public String toString() {
        int num = frac.getNumerateur();
        int den = frac.getDenominateur();
        if (num == 0) return "0";
        String msg = "";
        if (num != 1 && num != -1) msg += num;
        else if (den !=1 || pow == 0) msg += num;
        else if (num == -1 && den == 1) msg += "-";

        if (den != 1) msg += "/"+den;
        
        if (pow > 0) msg += Lexeme.getSType(Lexeme.VAR);
        if (pow > 1) msg += "^"+pow;
        return msg;
    }

}
