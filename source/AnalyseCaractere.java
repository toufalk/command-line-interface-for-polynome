/**
 * Classe d'analyse alphabétique.
 * 
 * <p>
 * Cette classe fait l'analyse alphabétique de la chaîne de compilation.
 * Elle s'assure que tous les caractères utilisés dans l'expression sont valides.
 * Elle transforme la chaîne de caractères en liste de lexèmes.
 * </p>
 * 
 * <p>
 * Elle n'analyse pas tout d'un bloc mais en flux tendu : elle produit les lexèmes au fur et à mesure qu'on lui demande.
 * Ceci permet de faire le traitement en une seule passe.
 * </p>
 */
public class AnalyseCaractere {
    
    /** la chaîne à analyser. */
    private String chaine;
    
    /** l'indice de travail dans la chaîne de caractère.
     * C'est l'indice du prochain lexème à fournir.
     */
    private int indice;
    
    /**
     * lexème en cours de lecture.
     * On l'a généré mais on ne l'a pas encore consommé.
     */
    private Lexeme next;
    
    /**
     * Indique si on est en fin d'analyse.
     * finChaine vaut <code>true</code> si on a atteint la partie commentaire
     * ou si on a atteint la fin de la chaîne.
     */
    private boolean finChaine;



    /**
     * Création de l'objet d'analyse de caractère.
     * 
     * @param cmd  la chaîne à analyser
     */
    public AnalyseCaractere(String cmd) {
        chaine = cmd;
        indice = 0;
        next = null;
        finChaine = false;
    }


    
    /**
     * Retourne le type du prochain lexème.
     * <p>
     * Cette méthode ne consomme pas le lexème mais indique simplement son type.
     * </p>
     * 
     * @return le type du prochain lexème
     * 
     * @throws AnalyseException  si le prochain caractère n'est pas autorisé par le langage
     */
    public int getNextLexemeType() throws AnalyseException {
        if (next == null) 
            next = getNextLexeme();
        if (next == null)
            return Lexeme.ERR;
        return next.getType();
    }

    
    /**
     * Retourne le prochain lexème.
     * <p>
     * Cette méthode consomme le lexème renvoyé.
     * </p>
     * 
     * @return le prochain lexème analysé
     * 
     * @throws AnalyseException  si le prochain caractère n'est pas autorisé par le langage
     */
    public Lexeme getNextLexeme() throws AnalyseException {
        Lexeme lex = null;
        if (next != null) {
            lex = next;
            next = null;
            return lex;
        }
        if (finChaine) return null;
        if (indice < chaine.length()) {
            char lastChar = chaine.charAt(indice);
            if ('0' <= lastChar && lastChar <= '9')
                lex = new Lexeme(Lexeme.INT,lastChar);
            if ('a' <= lastChar && lastChar <= 'z')
                lex = new Lexeme(Lexeme.IDF, lastChar);
            if ('A' <= lastChar && lastChar <= 'Z')
                lex = new Lexeme(Lexeme.MAJ, lastChar);
            switch(lastChar) {
            case '+': lex = new Lexeme(Lexeme.ADD); break;
            case '-': lex = new Lexeme(Lexeme.SUB); break;
            case '*': lex = new Lexeme(Lexeme.MUL); break;
            case '/': lex = new Lexeme(Lexeme.DIV); break;
            case '^': lex = new Lexeme(Lexeme.POW); break;
            case '(': lex = new Lexeme(Lexeme.PAO); break;
            case ')': lex = new Lexeme(Lexeme.PAF); break;
            case 'X': lex = new Lexeme(Lexeme.VAR); break;
            case '=': lex = new Lexeme(Lexeme.EGA); break;
            case ',': lex = new Lexeme(Lexeme.VIR); break;
            case '_': lex = new Lexeme(Lexeme.IDF, lastChar); break;
            case '\t':
            case ' ': lex = new Lexeme(Lexeme.ESP); break;
            case ';': lex = new Lexeme(Lexeme.END); finChaine = true; break;
            }
            if (lex == null) throw new AnalyseException("caractère " + lastChar + " non autorisé.");
        } else {
            lex = new Lexeme(Lexeme.END);
            finChaine = true;
        }
        indice++;
        return lex;
    }


}
