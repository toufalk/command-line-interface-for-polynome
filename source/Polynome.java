/**
 * Classe représentant des polynômes.
 *
 * <p>
 * Ce sont des polynômes à coefficients rationnels.
 * </p>
 *
 * <p>
 * Ils sont stockés sous forme développée et réduite. L'ordre des terme n'est par contre pas spécifié.
 * En interne, un polynôme est une liste doublement chaînée de monômes.
 * </p>
 *
 * <p>
 * Les objets polynômes sont immutables : ils ne peuvent pas être modifiés.
 * Toutes les opérations mathématiques dessus (addition, multiplication, puissance,...)
 * devront donc créer de nouveaux objets.
 * </p>
 * 
 * @see Monome
 */
public class Polynome {

    /** premier terme de la chaîne de monômes. */ 
    private Chaine premierTerme;

    /** dernier terme de la chaîne de monômes. */
    private Chaine dernierTerme;


    /**
     * Création du polynôme nul.
     */
    public Polynome() {
        premierTerme = null;
        dernierTerme = null;
    }


    /**
     * Création d'un polynôme constitué d'un seul monôme.
     *
     * @param m  le monôme.
     */
    public Polynome(Monome m) {
	this();
	if (!m.estNul()) {
	    premierTerme = new Chaine(m);
	    dernierTerme = premierTerme;
	}
    }

    

    /**
     * Teste si le polynôme est nul.
     *
     * @return <code>true</code> si le polynôme est nul, <code>false</code> sinon.
     */
    public boolean estNul() {
        return (premierTerme == null);
    }


    
    /**
     * Clone le polynôme.
     *
     * <p>
     * Cette méthode est utile pour toutes les opérations subies par les polynômes.
     * </p>
     *
     * @return une copie intégrale et fidèle du polynôme.
     */
    public Polynome clone() {
        Chaine newcur = null;
        Chaine newp = null;
        try {
            Chaine cur = premierTerme;
            while (cur != null) {
                Monome m = cur.getMonome();
                Monome newm = new Monome(m.getNumerateur(), m.getDenominateur(), m.getPuissance());
                newcur = new Chaine(newcur, newm, null);
                if (newp == null) newp = newcur;
                cur = cur.getNext();
            }
        } catch(AnalyseException ae) {}
        Polynome p = new Polynome();
        p.premierTerme = newp;
        p.dernierTerme = newcur;
        return p;
    }


    /**
     * simplifie le polynôme.
     *
     * <p>
     * Rassemble les monômes de même degré ensemble et supprime ceux qui sont nuls.
     * </p>
     */
    private void simplifie() {
        Chaine cur = premierTerme;
        while (cur != null) {
            // On parcours tous les monomes à la recherche de simplification
            int num = cur.getMonome().getNumerateur();
            int den = cur.getMonome().getDenominateur();
            int pow = cur.getMonome().getPuissance();
            Chaine mob = cur.getNext();
            while (mob != null) {
                // Est-ce que le monome «mobile» est de même puissance que celui en cours ?
                if (mob.getMonome().getPuissance() == pow) {
                    // oui, donc on les somme.
                    num = num*mob.getMonome().getDenominateur() + den*mob.getMonome().getNumerateur();
                    den = den*mob.getMonome().getDenominateur();
                    // et on fait disparaitre ce monome de la somme
                    Chaine.concatene(mob.getPrevious(), mob.getNext());
                    if (mob == dernierTerme) dernierTerme = mob.getPrevious();
                }
                mob = mob.getNext();
            }
            // si le monome courant est nul, on supprime
            if (num == 0) {
                if (cur == premierTerme && cur == dernierTerme) {
                    premierTerme = null;
                    dernierTerme = null;
                } else {
                    Chaine.concatene(cur.getPrevious(), cur.getNext());
                    if (cur == premierTerme) premierTerme = cur.getNext();
                    if (cur == dernierTerme) dernierTerme = cur.getPrevious();
                }
            } else {
                // S'il avait des copains, on le MàJ
                if (num != cur.getMonome().getNumerateur()) {
                    try {
                        cur = new Chaine(cur.getPrevious(), new Monome(num, den, pow), cur.getNext());
                        if (cur.getPrevious() == null) premierTerme = cur;
                        if (cur.getNext() == null) dernierTerme = cur;
                    } catch(AnalyseException ae) {}
                }
            }
            // et on passe au suivant
            cur = cur.getNext();
        }
    }
    

    /**
     * Retourne une représentation du polynôme sous forme de chaîne de caractère.
     *
     * <p>
     * Cette méthode s'appuie sur la représentation des monômes.
     * </p>
     *
     * @return une représentation du polynôme sous forme de chaîne de caractères.
     *
     * @see Monome#toString
     */
    public String toString() {
        if (estNul()) return "0";
        String msg = "";
        Chaine c = premierTerme;
        while (c != null) {
            Monome m = c.getMonome();
            if (!m.estNul()) {
                if (msg.length() >0) {
                    if (m.getNumerateur() > 0) msg += " +";
                    else msg += " ";
                }
                msg += m;
            }
            c = c.getNext();
        }
        return msg;
    }

    

    /*----------------*\
     *    Addition    *
    \*----------------*/

    /**
     * Calcule la somme du polynome <code>p</code> et de l'entier <code>n</code>.
     *
     * @param p  le polynôme.
     * @param n  l'entier.
     *
     * @return la somme <code>p + n</code>
     */
    public static Polynome add(Polynome p, int n) {
	return add(p, new Monome(n));
    }


    /**
     * Calcule la somme du polynome <code>p</code> et de la fraction <code>f</code>.
     *
     * @param p  le polynome.
     * @param f  la fraction.
     *
     * @return la somme <code>p + f</code>.
     */
    public static Polynome add(Polynome p, Fraction f) {
	return add(p, new Monome(f));
    }


    /**
     * Calcule la somme du polynôme <code>p</code> et du monôme <code>m</code>.
     *
     * @param p  la polynome.
     * @param m  le monome.
     *
     * @return la somme <code>p + m</code>.
     */
    public static Polynome add(Polynome p, Monome m) {
        return add(p, new Polynome(m));
    }
    

    /**
     * Calcule la somme des polynômes <code>p1</code> et <code>p2</code>.
     *
     * @param p1  le premier polynôme.
     * @param p2  le deuxième polynome.
     *
     * @return la somme <code>p1 + p2</code>.
     */
    public static Polynome add(Polynome p1, Polynome p2) {
        if (p1.estNul()) return p2.clone();
        if (p2.estNul()) return p1.clone();
        Polynome cp1 = p1.clone();
        Polynome cp2 = p2.clone();
        Chaine.concatene(cp1.dernierTerme, cp2.premierTerme);
        cp1.dernierTerme = cp2.dernierTerme;
	cp1.simplifie();
        return cp1;
    }



    /*----------------------*\
     *    Multiplication    *
    \*----------------------*/

    /**
     * Calcule le produit du polynome <code>p</code> et de l'entier <code>n</code>.
     *
     * @param p  le polynôme.
     * @param n  l'entier.
     *
     * @return le produit <code>p * n</code>
     */
    public static Polynome mult(Polynome p, int n) {
	return mult(p, new Monome(n));
    }


     /**
     * Calcule le produit du polynome <code>p</code> et de la fraction <code>f</code>.
     *
     * @param p  le polynome.
     * @param f  la fraction.
     *
     * @return le produit <code>p * f</code>.
     */
    public static Polynome mult(Polynome p, Fraction f) {
	return mult(p, new Monome(f));
    }
    

    /**
     * Calcule le produit du polynôme <code>p</code> et du monôme <code>m</code>.
     *
     * @param p  la polynome.
     * @param m  le monome.
     *
     * @return le produit <code>p * m</code>.
     */
    public static Polynome mult(Polynome p, Monome m) {
        return mult(p, new Polynome(m));
    }
    

    /**
     * Calcule le produit des polynômes <code>p1</code> et <code>p2</code>.
     *
     * @param p1  le premier polynôme.
     * @param p2  le deuxième polynome.
     *
     * @return le produit <code>p1 * p2</code>.
     */
    public static Polynome mult(Polynome p1, Polynome p2) {
        if (p1.estNul() || p2.estNul()) return new Polynome();
        Polynome cp1 = p1.clone();
        Polynome res = new Polynome();
        try {
            Chaine ccp1 = cp1.premierTerme;
            while (ccp1 != null) {
                Polynome cp2 = p2.clone();
                Monome m1 = ccp1.getMonome();
                Chaine ccp2 = cp2.premierTerme;
                while (ccp2 != null) {
                    Monome m2 = ccp2.getMonome();
                    Monome mres = new Monome(m1.getNumerateur()*m2.getNumerateur(),
                                             m1.getDenominateur()*m2.getDenominateur(),
                                             m1.getPuissance()+m2.getPuissance());
                    if (res.premierTerme == null) {
                        res.premierTerme = new Chaine(mres);
                        res.dernierTerme = res.premierTerme;
                    } else {
                        res.dernierTerme = new Chaine(res.dernierTerme, mres, null);
                    }
                    ccp2 = ccp2.getNext();
                }
                ccp1 = ccp1.getNext();
            }
        } catch(AnalyseException ae) {}
	res.simplifie();
        return res;
    }




    /*-----------------*\
     *    Puissance    *
    \*-----------------*/

    /**
     * Calcule la puissance du polynôme.
     *
     * @param p  le polynôme.
     * @param n la puissance.
     *
     * @return le polynôme <code>p^n</code>
     */
    public static Polynome pow(Polynome p, int n) {
        if (n == 0) return new Polynome(Monome.UN);
        if (n == 1) return p.clone();
        Polynome pp = pow(p, n>>1);
        pp = Polynome.mult(pp, pp);
        if ((n&1) == 0) return pp;
        else return Polynome.mult(pp, p);
    }




    /*-----------------*\
     *    Fonctions    *
    \*-----------------*/

    /**
     * Calcul du degré d'un polynôme.
     *
     * <p>
     * Il y a une différence avec le degré mathématique :
     * En maths, le degré du polynôme nul est -infini.
     * L'informatique ne connaissant pas cette valeur, le degré du polynôme nul
     * ici vaudra zéro, comme n'importe quel polynôme constant.
     * </p>
     *
     * @return le degré du polynôme.
     */
    public int getDegre() {
        int k=0;
        int p;
        Chaine chaine = premierTerme;
        while (chaine != null) {
            p = chaine.getMonome().getPuissance();
            if (k < p) k = p;
            chaine = chaine.getNext();
        }
        return k;
    }



    /**
     * Calcul du quotient de la division euclidienne.
     *
     * <p>
     * Il s'agit du polynôme pQ tel que pA = pQ * pB + pR avec deg(pR)&lt;deg(pB)
     * </p>
     *
     * @param pA polynôme à diviser.
     * @param pB polynôme diviseur.
     *
     * @return le quotient de le division euclidienne.
     *
     * @throws AnalyseException  si le polynome pB est nul.
     */
    public static Polynome quoDivEucl(Polynome pA, Polynome pB) throws AnalyseException {
	if (pB.estNul())
	    throw new AnalyseException("division de polynôme par zéro.");
        // A = B * Q + R
        int m = pA.getDegre();
        int n = pB.getDegre();
        // Si deg(A) < deg(B) alors QUO = 0
	// Si A == 0 alors QUO = 0
        if ((m < n) || (pA.estNul())) return new Polynome();

	Chaine c = pA.premierTerme;
        // terme du plus haut degré de A : amX^m = (amn/amd)X^m
        int amn=0, amd=0;
        while (c != null) {
            if (c.getMonome().getPuissance() == m) {
                amn = c.getMonome().getNumerateur();
                amd = c.getMonome().getDenominateur();
                break;
            }
        }
        c = pB.premierTerme;
        // terme du plus haut degré de B : bnX^n = (bnn/bnd)X^n
        int bnn=0, bnd=0;
        while (c != null) {
            if (c.getMonome().getPuissance() == n) {
                bnn = c.getMonome().getNumerateur();
                bnd = c.getMonome().getDenominateur();
                break;
            }
        }
        Polynome aA = new Polynome(new Monome(amn*bnd,amd*bnn,m-n));
        // Q' = QUO( A - (am/bn)X^(m-n) B , B)
        Polynome Qprime = quoDivEucl(add(pA,mult(mult(pB,-1),aA)), pB);
        // QUO = Q' + (am/bn)X^(m-n)
        return add(Qprime, aA);
    }



    /**
     * Calcul du reste de la division euclidienne.
     *
     * <p>
     * Il s'agit du polynôme pR tel que pA = pQ * pB + pR avec deg(pR)&lt;deg(pB)
     * </p>
     *
     * @param pA polynôme à diviser.
     * @param pB polynôme diviseur.
     *
     * @return le reste de le division euclidienne.
     *
     * @throws AnalyseException  si le polynome pB est nul.
     */
    public static Polynome remDivEucl(Polynome pA, Polynome pB) throws AnalyseException {
	if (pB.estNul())
	    throw new AnalyseException("division de polynôme par zéro.");
        // A = B * Q + R
        int m = pA.getDegre();
        int n = pB.getDegre();
        // Si deg(A) < deg(B) alors REM = A
        if (m < n) return pA.clone();
        // Si A == 0 alors REM = 0
        if (pA.estNul()) return new Polynome();
        Chaine c = pA.premierTerme;
        // terme du plus haut degré de A : amX^m = (amn/amd)X^m
        int amn=0, amd=0;
        while (c != null) {
            if (c.getMonome().getPuissance() == m) {
                amn = c.getMonome().getNumerateur();
                amd = c.getMonome().getDenominateur();
                break;
            }
        }
        c = pB.premierTerme;
        // terme du plus haut degré de B : bnX^n = (bnn/bnd)X^n
        int bnn=0, bnd=0;
        while (c != null) {
            if (c.getMonome().getPuissance() == n) {
                bnn = c.getMonome().getNumerateur();
                bnd = c.getMonome().getDenominateur();
                break;
            }
        }
        // REM(A,B) = REM( A - (am/bn)X^(m-n) B , B)
        return remDivEucl(add(pA,mult(pB,new Monome(-amn*bnd,amd*bnn,m-n))), pB);
    }
}
