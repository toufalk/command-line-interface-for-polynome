import java.util.Scanner;
import java.util.NoSuchElementException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


/**
 * Classe principale du projet.
 *
 * <p>
 * Elle signifie Command Line Interface for Polynoms.
 * </p>
 *
 * <p>
 * Elle permet de faire des calculs sur les polynômes à coefficients rationnels de manière littérale.
 * L'écriture des polynômes est aussi proche de l'écriture conventionnelle des mathématiques que possible.
 * </p>
 *
 * <p>
 * Voir le constructeur {@link #CLIP(String[]) CLIP} pour la syntaxe de l'utilisation.
 * </p>
 */
public class CLIP {


    /** Flux de données sur lequel lire les expressions à analyser. */
    private InputStream stream;

    /** Permet de spécifier si l'arbre abstrait doit être affiché. */
    private boolean afficheAA;

    /**
     * Permet de spécifier si l'expression lue doit être affichée avant analyse.
     * Ceci est utile, notamment lorsque le flux d'expression est un fichier. 
     * Cela permet de savoir quelle ligne est traitée
     */
    private boolean afficheExp;

    /** Permet de spécifier si une erreur provoque l'arrêt du programme. */
    private boolean quitSiErreur;


    
    /**
     * Construit un objet CLIP.
     * <p>
     * Cette méthode sert principalement à gérer les différentes options possibles du programme.
     * </p>
     *
     * <p>
     * Options implémentées :
     * <dl>
     * <dt>-h</dt><dd> : affiche une aide et quitte le programme.</dd>
     * <dt>-a</dt><dd> : affiche l'arbre abstrait de chaque expression analysée correctement.</dd>
     * <dt>-i</dt><dd> : mode interactif. Le programme attend les expressions sur son entrée standard afin de les
     * analyser. La fin est provoquée par l'arrêt du programme [ctrl-C], une fin de flux [ctrl-D] ou l'expression
     * <code>quit</code> seule sur une ligne.</dd>
     * <dt>-f ficname</dt><dd> : mode fichier. Le programme lit ses expressions depuis le fichier <code>ficname</code>.
     * Les lignes sont lues et passées une par une à l'analyseur. Le programme s'arrête en fin de fichier.</dd>
     * <dt>-c exp exp ...</dt><dd> : mode commande. Le programme considère tous les paramètres suivants l'option
     * <code>-c</code> comme étant des expressions à analyser. Attention dans ce cas aux espaces !
     * Ils servent en effet de délimiteur de paramètres. Pour écrire des espaces dans les expressions, vous pouvez
     * soit échapper l'espace, c'est-à-dire écrire un antislash avant l'espace, soit entourer l'expression de
     * guillemets : <code>X\ +1</code> ou <code> "X +1"</code>.</dd>
     * </dl>
     * Par nature, les options de mode <code>-i</code>, <code>-f</code> et <code>-c</code> s'excluent mutuellement.
     * C'est la première qui est trouvée qui est exécutée.
     * </p>
     *
     * <p>
     * Il est préférable que l'option <code>-a</code> soit avant les options de mode. 
     * </p>
     * 
     * @param args  arguments contenant les paramètres et/ou les expressions
     * 
     */
    public CLIP(String[] args) {
        afficheExp = true;
        afficheAA = false;
        quitSiErreur = true;
        boolean done = false;
        // gestion des paramètres
        for (int k=0; k<args.length; k++) {
            if ("-h".equals(args[k])) {
                aide();
                System.exit(0);
            }
            if ("-a".equals(args[k])) {
                afficheAA = true;
            }
            if (!done) {
                if ("-f".equals(args[k])) {
                    System.out.println("lecture du fichier : " + args[k+1]);
                    try {
                        stream = new FileInputStream(args[k+1]);
                        done = true;
                    } catch(FileNotFoundException fnfe) {
                        System.err.println("Fichier non trouvé");
                        System.exit(1);
                    }
                }
                if ("-c".equals(args[k])) {
                    System.out.println("lecture de la liste de commandes");
                    StringBuilder sb = new StringBuilder();
                    for (int j=k+1; j<args.length; j++) {
                        sb.append(args[j]);
                        sb.append("\n");
                    }
                    try {
                        stream = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
                    } catch(UnsupportedEncodingException uee) {
                        System.err.println("problème d'encodage.");
                        System.exit(2);
                    }
                    done = true;
                }
                if ("-i".equals(args[k])) {
                    System.out.println("mode interactif");
                    stream = System.in;
                    afficheExp = false;
                    quitSiErreur = false;
                    done = true;
                }
            }
        }
        // mode interactif si rien de précisé
        if (!done) {
            System.out.println("pas d'option. Passage en mode interactif.");
            stream = System.in;
            afficheExp = false;
            quitSiErreur = false;
        }
    }


    /**
     * Envoie l'expression aux différentes analyses et affiche le résultat ou l'erreur d'analyse.
     *
     * <p>
     * Elle crée successivement une {@link AnalyseCaractere}, une {@link AnalyseLexicale}, une {@link AnalyseSyntaxique}
     * et une {@link AnalyseSemantique}. Le résultat est affiché. En cas d'erreur, l'{@link AnalyseException}
     * est attrapée et gérée.
     * </p>
     *
     * @param str  expression à analyser
     */    
    private void manageLine(String str) {
        try {
            if (afficheExp) System.out.println(str);
            AnalyseCaractere analcar = new AnalyseCaractere(str);
            AnalyseLexicale analex = new AnalyseLexicale(analcar);
            AnalyseSyntaxique analsynt = new AnalyseSyntaxique(analex);
            AnalyseSemantique analsem = new AnalyseSemantique(analsynt);
            int type = analsem.getTypeRetour();
            analsem.calculeRetour();
            
            if (afficheAA) System.out.println(analsynt.getArbreAbstrait());
            System.out.print("ANS = ");
            if (type == Variable.TYPE_VOID) System.out.println("");
            if (type == Variable.TYPE_ENT)  System.out.println(analsem.getIntValue());
            if (type == Variable.TYPE_FRAC) System.out.println(analsem.getFracValue());
            if (type == Variable.TYPE_POLY) System.out.println(analsem.getPolyValue());
                        
        } catch (AnalyseException ce) {
            System.err.println("ERREUR !!");
            System.err.println(ce.getMessage());
            if (quitSiErreur) System.exit(5);
        }
    }


    /**
     * Lit sur le flux de données les expressions une par une et les envoie en traitement à
     * la méthode {@link #manageLine(String) manageLine}.
     *
     * <p>
     * Cette méthode gère la fin du flux de données.
     * </p>
     */
    private void goGoPoly() {
        boolean fin = false;
        Scanner scan = new Scanner(stream);
        String str;
        try {
            while (!fin) {
                System.out.print("\n?: ");
                str = scan.nextLine();
                if ("quit".equals(str)) fin = true;
                else manageLine(str);
            }
        } catch (NoSuchElementException nsee) {
            System.out.println("quit");
        } catch (IllegalStateException ise) {
            System.out.println("quit");
        }
    }


    /**
     * Affiche une aide en ligne de commande.
     */
    public void aide() {
        System.out.println("Aide de CLIP");
        System.out.println("java CLIP [-a] [-h] [-i] [-f ficname] [-c cmd [cmd...]]");
        System.out.println("options:");
        System.out.println("\t-a: affichage de l'arbre abstrait");
        System.out.println("\t-h: affiche cette aide et quit");
        System.out.println("\t-i: mode interactif");
        System.out.println("\t-f: lit ses instructions depuis le ficher ficname");
        System.out.println("\t-c: le reste des paramètres de la ligne de commande");
        System.out.println("\t     sont les instructions");
        System.out.println("Les options -i, -f et -c s'excluent mutuellement.");
        System.out.println("S'il y en a plusieurs, c'est la première qui l'emporte.");
    }




    
    /**
     * Méthode principale du projet.
     * <p>
     * C'est elle qu'il faut appeler depuis la ligne de commande.
     * Voir le constructeur {@link #CLIP(String[]) CLIP} pour plus de précision.
     * </p>
     *
     * @param args  les arguments de la ligne de commande
     */
    public static void main(String[] args) {
        CLIP clip = new CLIP(args);
        clip.goGoPoly();
    }
    
}
