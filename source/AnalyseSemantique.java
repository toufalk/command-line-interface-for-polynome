/**
 * Analyse sémantique des polynômes.
 *
 * <p>
 * Cette analyse se base sur l'arbre abstrait crée par l'analyseur syntaxique.
 * Elle se passe en deux temps : d'une part l'analyse sémantique statique et d'autre part
 * l'analyse sémantique dynamique.
 * </p>
 *
 * <p>
 * L'analyse statique consiste à vérifier que les types de chaque opérande de chaque noeud de 
 * l'arbre abstrait sont cohérents. Elle vérifie aussi que les identifiants de variables ont bien
 * été déclarés une et une seule fois.
 * </p>
 *
 * <p>
 * L'analyse dynamique construit le polynôme. Elle s'assure que les dénominateurs sont non-nuls et
 * que les puissances sont des entiers positifs.
 * </p>
 */
class AnalyseSemantique {


    /** Arbre abstrait à analyser. */
    private ArbreAbstrait arbre;

    /**
     * Entier représentant le type du résultat obtenu après génération.
     * @see Variable
     */
    private int typeResult;

    /** la variable résultat obtenu après génération. */
    private Variable resultat;



    /**
     * Préparation des analyses.
     *
     * @param as  analyseur syntaxique qui fournit l'arbre abstrait à analyser
     */
    public AnalyseSemantique(AnalyseSyntaxique as) {
        arbre = as.getArbreAbstrait();
        typeResult = -1;
        resultat = null;
    }
    

    /**
     * Analyse sémantique statique
     *
     * @return un entier représentant le type du résultat qui serait obtenu après génération.
     *
     * @throws AnalyseException  si un type n'est pas cohérent dans l'arbre abstrait.
     */
    public int getTypeRetour() throws AnalyseException {
        if (typeResult == -1) typeResult = getType(arbre);
        return typeResult;
    }


    /**
     * Méthode récurssive d'analyse sémantique statique.
     *
     * @param aa  arbre abstrait qui sera analysé
     *
     * @return un entier représentant le type du résultat qui serait obtenu après génération.
     *
     * @throws AnalyseException  si un type n'est pas cohérent dans l'arbre abstrait.
     */
    private int getType(ArbreAbstrait aa) throws AnalyseException {
        int tg, td;
        switch(aa.getLabel()) {

        case ArbreAbstrait.ADDIT:
        case ArbreAbstrait.SUBST:
        case ArbreAbstrait.MULTI:
            tg = getType(aa.getAAGauche());
            td = getType(aa.getAADroit());
            if (tg == Variable.TYPE_POLY) {
                if ((td == Variable.TYPE_ENT) ||
                    (td == Variable.TYPE_FRAC) ||
                    (td == Variable.TYPE_POLY))
                    return Variable.TYPE_POLY;
            }
            if (tg == Variable.TYPE_ENT) {
                if (td == Variable.TYPE_ENT) return Variable.TYPE_ENT;
                if (td == Variable.TYPE_FRAC) return Variable.TYPE_FRAC;
                if (td == Variable.TYPE_POLY) return Variable.TYPE_POLY;
            }
            if (tg == Variable.TYPE_FRAC) {
                if (td == Variable.TYPE_ENT) return Variable.TYPE_FRAC;
                if (td == Variable.TYPE_FRAC) return Variable.TYPE_FRAC;
                if (td == Variable.TYPE_POLY) return Variable.TYPE_POLY;
            }
            throw new AnalyseException("Erreur de typage: " +
                                       Variable.getSType(tg) + " " + aa.getSLabel() + " " + Variable.getSType(td));

        case ArbreAbstrait.DIVID:
            tg = getType(aa.getAAGauche());
            td = getType(aa.getAADroit());
            if ((td == Variable.TYPE_ENT) ||
                (td == Variable.TYPE_FRAC)) {
                if (tg == Variable.TYPE_POLY)
                    return Variable.TYPE_POLY;
                if ((tg == Variable.TYPE_ENT) ||
                    (tg == Variable.TYPE_FRAC))
                    return Variable.TYPE_FRAC;
            }
            throw new AnalyseException("Erreur de typage: " +
                                       Variable.getSType(tg) + " " + aa.getSLabel() + " " + Variable.getSType(td));

        case ArbreAbstrait.POWER:
            tg = getType(aa.getAAGauche());
            td = getType(aa.getAADroit());
            if (td == Variable.TYPE_ENT) {
                if (tg == Variable.TYPE_POLY) return Variable.TYPE_POLY;
                if (tg == Variable.TYPE_ENT) return Variable.TYPE_ENT;
                if (tg == Variable.TYPE_FRAC) return Variable.TYPE_FRAC;
            }
            throw new AnalyseException("Erreur de typage: " +
                                       Variable.getSType(tg) + " " + aa.getSLabel() + " " + Variable.getSType(td));

        case ArbreAbstrait.MINUS:
            tg = getType(aa.getAAGauche());
            if ((tg == Variable.TYPE_POLY) ||
                (tg == Variable.TYPE_ENT) ||
                (tg == Variable.TYPE_FRAC)) return tg;
            throw new AnalyseException("Erreur de typage: " +
                                       aa.getSLabel() + " " + Variable.getSType(tg));

        case ArbreAbstrait.LEAFN:
            return Variable.TYPE_ENT;

        case ArbreAbstrait.LEAFX:
            return Variable.TYPE_POLY;

        case ArbreAbstrait.TYPAG:
            if ("INT".equals(aa.getSLeaf())) return Variable.TYPE_ENT;
            if ("FRAC".equals(aa.getSLeaf())) return Variable.TYPE_FRAC;
            if ("POLY".equals(aa.getSLeaf())) return Variable.TYPE_POLY;
            throw new AnalyseException("Erreur de typage: " + aa.getSLabel() + " " + aa.getSLeaf());

        case ArbreAbstrait.IDVAL:
            if (Environment.env.existIdentifiant(aa.getSLeaf()))
                return Environment.env.getTypeIdentifiant(aa.getSLeaf());
            throw new AnalyseException("Variable " + aa.getSLeaf() + " non déclarée");
            
        case ArbreAbstrait.IDAFF:
            if (Environment.env.existIdentifiant(aa.getSLeaf()))
                return Environment.env.getTypeIdentifiant(aa.getSLeaf());
            return Variable.TYPE_VOID;

        case ArbreAbstrait.DECLA:
            tg = getType(aa.getAAGauche());
            td = getType(aa.getAADroit());
            if (td == Variable.TYPE_VOID) {
                if ((tg == Variable.TYPE_ENT) ||
                    (tg == Variable.TYPE_FRAC)  ||
                    (tg == Variable.TYPE_POLY)) {
                    Environment.env.addIdentifiant(aa.getAADroit().getSLeaf(), tg);
                    return tg;
                }
            } else
                throw new AnalyseException("Variable " + aa.getAADroit().getSLeaf() + " déjà déclarée.");
            throw new AnalyseException("Erreur de typage: " +
                                       Variable.getSType(tg) + " " + aa.getSLabel() + " " + Variable.getSType(td));
            
        case ArbreAbstrait.AFFEC:
            tg = getType(aa.getAAGauche());
            td = getType(aa.getAADroit());
            if (tg == td)
                return tg;
            throw new AnalyseException("Erreur de typage: " +
                                       Variable.getSType(tg) + " " + aa.getSLabel() + " " + Variable.getSType(td));
            
        case ArbreAbstrait.FONCT:
            String fct = aa.getSLeaf();
            if ("DEG".equals(fct)) {
                tg = getType(aa.getAAGauche());
                if ((tg == Variable.TYPE_ENT) ||
                    (tg == Variable.TYPE_FRAC) ||
                    (tg == Variable.TYPE_POLY))
                    return Variable.TYPE_ENT;
                throw new AnalyseException("Erreur de typage: " + fct + "(" + Variable.getSType(tg) + ")");
            }
            if ("QUO".equals(fct) || 
                "REM".equals(fct)) {
                tg = getType(aa.getAAGauche());
                td = getType(aa.getAADroit());
                if (((tg == Variable.TYPE_ENT) ||
                     (tg == Variable.TYPE_FRAC) ||
                     (tg == Variable.TYPE_POLY)) &&
                    ((td == Variable.TYPE_ENT) ||
                     (td == Variable.TYPE_FRAC) ||
                     (td == Variable.TYPE_POLY)))
                    return Variable.TYPE_POLY;
                throw new AnalyseException("Erreur de typage: " + fct + "(" +
                                           Variable.getSType(tg) + ", " + Variable.getSType(td) + ")");
            }
            throw new AnalyseException("Erreur de typage: " + fct + " non typée.");
        }
        throw new AnalyseException("Erreur dans l'arbre abstrait: label inconnue.");
    }







    /**
     * Retourne la valeur du résultat s'il est de type entier.
     *
     * <p>
     * Le comportement de cette méthode est indéterminé si le résultat de l'analyse n'est pas de type entier.
     * </p>
     *
     * @return la valeur entière du résultat de l'analyse dynamique.
     *
     * @throws AnalyseException  s'il y a un problème pendant la génération.
     */
    public int getIntValue() throws AnalyseException {
        verifieEvaluation(Variable.TYPE_ENT);
        return resultat.getNbVal();
    }


    /**
     * Retourne la valeur du résultat s'il est de type fraction.
     *
     * <p>
     * Le comportement de cette méthode est indéterminé si le résultat de l'analyse n'est pas de type fraction.
     * </p>
     *
     * @return la valeur fractionnaire du résultat de l'analyse dynamique.
     *
     * @throws AnalyseException  s'il y a un problème pendant la génération.
     */
    public Fraction getFracValue() throws AnalyseException {
        verifieEvaluation(Variable.TYPE_FRAC);
        return resultat.getFracVal();
    }


    /**
     * Retourne la valeur du résultat s'il est de type polynôme.
     *
     * <p>
     * Le comportement de cette méthode est indéterminé si le résultat de l'analyse n'est pas de type polynôme.
     * </p>
     *
     * @return la valeur polynomiale du résultat de l'analyse dynamique.
     *
     * @throws AnalyseException  s'il y a un problème pendant la génération.
     */
    public Polynome getPolyValue() throws AnalyseException {
        verifieEvaluation(Variable.TYPE_POLY);
        return resultat.getPolyVal();
    }


    /**
     * Génère la valeur du résultat de l'expression analysée.
     *
     * Cette méthode met aussi à jour la variable d'environnement <code>ANS</code> (type et valeur).
     *
     * @throws AnalyseException  s'il y a un problème pendant la génération.
     */
    public void calculeRetour() throws AnalyseException {
        if (resultat == null) {
            resultat = evaluate(arbre);
            Environment.env.addIdentifiant("ANS", resultat.getType());
            Memoire.mem.setValeur("ANS", resultat);
        }
    }

    
    /**
     * Vérifie que le type du résultat est bien conforme à ce qu'on veut.
     *
     * <p>
     * Le résultat est généré si besoin.
     * </p>
     *
     * @param type  le type souhaité du résultat de la génération.
     *
     * @throws AnalyseException  si le type du résultat n'est pas celui souhaité.
     */
    private void verifieEvaluation(int type) throws AnalyseException {
        if (typeResult == -1) getTypeRetour();
        if (typeResult != type)
            throw new AnalyseException("L'expression n'est pas de type " + Variable.getSType(type) + ".");
        calculeRetour();
    }


    /**
     * Évalue la valeur du résultat d'un arbre abstrait.
     *
     * <p>
     * Il s'agit du coeur de l'analyse sémantique dynamique.
     * C'"est une fonction récurssive.
     * </p>
     *
     * @param aa  l'arbre abstrait qui sert de base à la génération du résultat.
     *
     * @return le résultat de la génération de l'expression.
     *
     * @throws AnalyseException  si un problème de génération survient dans l'analyse sémantique.
     */
    private Variable evaluate(ArbreAbstrait aa) throws AnalyseException {
        Variable vg, vd;
        switch(aa.getLabel()) {

        case ArbreAbstrait.ADDIT:
            vg = evaluate(aa.getAAGauche());
            vd = evaluate(aa.getAADroit());
            return Variable.addition(vg,vd);
            
        case ArbreAbstrait.SUBST:
            vg = evaluate(aa.getAAGauche());
            vd = evaluate(aa.getAADroit());
            return Variable.soustraction(vg,vd);
            
        case ArbreAbstrait.MULTI:
            vg = evaluate(aa.getAAGauche());
            vd = evaluate(aa.getAADroit());
            return Variable.multiplication(vg,vd);
            
        case ArbreAbstrait.DIVID:
            vg = evaluate(aa.getAAGauche());
            vd = evaluate(aa.getAADroit());
            return Variable.division(vg,vd);

        case ArbreAbstrait.POWER:
            vg = evaluate(aa.getAAGauche());
            vd = evaluate(aa.getAADroit());
            return Variable.puissance(vg,vd);

        case ArbreAbstrait.MINUS:
            vg = evaluate(aa.getAAGauche());
            return Variable.oppose(vg);

        case ArbreAbstrait.LEAFN:
            return new Variable(Integer.parseInt(aa.getSLeaf()));

        case ArbreAbstrait.LEAFX:
            return new Variable(new Polynome(Monome.X));

        case ArbreAbstrait.TYPAG:
            return new Variable(aa.getSLeaf());

        case ArbreAbstrait.IDAFF:
            return new Variable(aa.getSLeaf());

        case ArbreAbstrait.IDVAL:
            if (Memoire.mem.hasValeur(aa.getSLeaf()))
                return Memoire.mem.getValeur(aa.getSLeaf());
            throw new AnalyseException("Variable " + aa.getSLeaf() + " non initialisée");
            
        case ArbreAbstrait.DECLA:
            evaluate(aa.getAAGauche());
            return evaluate(aa.getAADroit());
            
        case ArbreAbstrait.AFFEC:
            String idf = evaluate(aa.getAAGauche()).getStrVal();
            vd = evaluate(aa.getAADroit());
            Memoire.mem.setValeur(idf, vd);
            return vd;
            
        case ArbreAbstrait.FONCT:
            String fct = aa.getSLeaf();
            if ("DEG".equals(fct)) {
                vg = evaluate(aa.getAAGauche());
                return Variable.getDegre(vg);
            }
            if ("QUO".equals(fct)) {
                vg = evaluate(aa.getAAGauche());
                vd = evaluate(aa.getAADroit());
                return Variable.getQuoDivEucl(vg, vd);
            }
            if ("REM".equals(fct)) {
                vg = evaluate(aa.getAAGauche());
                vd = evaluate(aa.getAADroit());
                return Variable.getRemDivEucl(vg, vd);
            }
            
        }
        throw new AnalyseException("Erreur dans l'arbre abstrait: label inconnue.");
    }


}
