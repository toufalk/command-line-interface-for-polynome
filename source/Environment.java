import java.util.Hashtable;

/**
 * Environnement de travail des polynômes.
 *
 * <p>
 * Cet environnement stocke les associations nom-type des variables.
 * Ceci permet de savoir qu'une variable a déjà été déclarée et son type.
 * </p>
 *
 * <p>
 * L'environnement est utilisé par l'analyse sémantique statique.
 * </p>
 *
 * <p>
 * Cette classe n'a pas de constructeur publique. Il n'est donc pas possible de créer des objets
 * <code>Environment</code>. On doit nécessairement passer par son instance statique <code>env</code>.
 * L'idée est que si on a besoin d'un nouvel environnement, il doit se poser sur la pile d'environnements
 * et la gestion de la pile doit se faire en interne au sein de la classe <code>Environment</code>.
 * </p>
 *
 * <p>
 * Pour l'instant, il n'est pas possible d'empiler les environnements car il n'existe pas encore de notion de blocs
 * pour les polynômes. C'est prévu. Dans ce cas, il existera une méthode statique pour créer un nouvel
 * environnement, celle-ci gérera l'empilement et le déempilement.
 * La portée des variables sera, quant à elle, gérée par la pile des environnements... Wait'n see !
 * </p>
 *
 * @see AnalyseSemantique
 */
public class Environment {

    /** L'envoronnement en cours. */
    public static Environment env = new Environment();


    /** la table de hachage contenant les associations nom-type. */
    private Hashtable<String, Integer> table;


    /**
     * Création d'un nouvel environnement.
     */
    private Environment() {
        table = new Hashtable<String, Integer>();
    }
    
    

    /**
     * Ajoute une association entre nom et type.
     *
     * <p>
     * Si le nom est dèjà associé à un type, l'ancienne association est remplacée par celle-ci.
     * </p>
     *
     * @param name  identifiant de la variable.
     * @param type  type de la variable.
     */
    public void addIdentifiant(String name, int type) {
        table.put(name, type);
    }


    /**
     * Donne l'exitence d'une association avec un nom de variable.
     *
     * @param name  identifiant de la variable.
     *
     * @return <code>true</code> si la variable <code>name</code> est déjà associée à un type, <code>false</code> sinon.
     */
    public boolean existIdentifiant(String name) {
        return table.containsKey(name);
    }


    /**
     * Donne le type associé à un nom de variable.
     *
     * @param name  identifiant de la variable.
     *
     * @return le type de la variable <code>name</code>.
     */
    public int getTypeIdentifiant(String name) {
        return table.get(name);
    }
}
