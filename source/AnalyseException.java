/**
 * Classe permettant de générer une erreur d'analyse.
 * <p>
 * Elle est utilisée par toutes les analyses du projet.
 * </p>
 */
public class AnalyseException extends Exception {

    /**
     * Constructeur de l'exception.
     * 
     * @param msg  message d'erreur pour décrire le problème
     */
    public AnalyseException(String msg) {
        super(msg);
    }
    
}
