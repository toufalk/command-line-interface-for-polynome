/**
 * Classe représentant une fraction.
 *
 * <p>
 * Une <code>Fraction</code> représente une fraction rationnelle, c'est à dire la division de deux entiers signés.
 * Elle est représentée par sa forme irréductible avec l'éventuel signe sur le numérateur.
 * </p>
 *
 * <p>
 * Les objets <code>Fraction</code> sont immutables, c'est-à-dire qu'on ne peut pas changer les valeurs
 * des numérateurs ou dénominateurs. On est obligé de créer un nouvel objet à chaque opération.
 * </p>
 *
 * <p>
 * Les opérations de base ont été implémentées en statique pour permettre une gestion plus facile des fractions.
 * </p>
 */
public class Fraction {

    /** numérateur de la fraction */
    private int num;

    /** dénominateur de la fraction */
    private int den;



    /**
     * Création d'une fraction.
     *
     * <p>
     * Le constructeur simplifie la fraction (la rend irréductible) et
     * passe le signe au numérateur si besoin.
     * </p>
     *
     * @param numerateur  le numérateur de la fraction.
     * @param denominateur  le dénominateur de la fraction.
     *
     * @throws AnalyseException  si le dénominateur est nul.
     */
    public Fraction(int numerateur, int denominateur) throws AnalyseException {
        num = numerateur;
        den = denominateur;

        if (den == 0)
            throw new AnalyseException("Dénominateur nul."); 
        if (num == 0) {
            den = 1;
        } else {
            // le signe de la fraction
            int sgn = (num*den < 0) ? -1 : 1;
            // les valeurs absolues
            num = (num < 0) ? -num : num;
            den = (den < 0) ? -den : den;
            int k=2;
            while (k<=num && k<=den) {
                if ((num%k == 0) && (den%k == 0)) {
                    num = num/k;
                    den = den/k;
                } else k++;
            }
            num = sgn*num;
        }
    }


    
    /**
     * Indique si la fraction est nulle.
     *
     * @return <code>true</code> si la fraction est nulle, <code>false</code> sinon.
     */
    public boolean estNulle() {
        return (num == 0);
    }


    /**
     * Retourne le numérateur de la fraction.
     *
     * @return le numérateur
     */
    public int getNumerateur() {
        return num;
    }


    /**
     * Retourne le dénominateur de la fraction.
     *
     * @return le dénominateur
     */
    public int getDenominateur() {
        return den;
    }



    /**
     * Retourne une représentation de la fraction sous forme de chaîne de caractères.
     *
     * <p>
     * Cette représentation respecte les standards des mathématiques :
     * <code>0/1 = 0</code> et <code>4/1=4</code>.
     * </p>
     *
     * @return une chaîne de caractères représentant la fraction.
     */
    public String toString() {
        if (num == 0) return "0";
        String str = "" + num;
        if (den > 1) str += "/" + den;
        return str;
    }

    

    

    /**
     * Additionne une fraction et un entier.
     *
     * @param frac  une fraction.
     * @param n  un entier.
     *
     * @return la somme de la fraction et de l'entier.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction add(Fraction frac, int n) throws AnalyseException {
        return new Fraction(frac.num + n*frac.den, frac.den);
    }

    
    /**
     * Additionne un entier et une fraction.
     *
     * @param n  un entier.
     * @param frac  une fraction.
     *
     * @return la somme de l'entier et de la fraction.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction add(int n, Fraction frac) throws AnalyseException {
        return new Fraction(frac.num + n*frac.den, frac.den);
    }


    /**
     * Additionne deux fractions.
     *
     * @param f1  une fraction.
     * @param f2  une fraction.
     *
     * @return la somme des deux fractions.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction add(Fraction f1, Fraction f2) throws AnalyseException {
        return new Fraction(f1.num*f2.den + f2.num*f1.den, f1.den*f2.den);
    }

    

    
    /**
     * Multiplie une fraction et un entier.
     *
     * @param frac  une fraction.
     * @param n  un entier.
     *
     * @return le produit de la fraction et de l'entier.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction mult(Fraction frac, int n) throws AnalyseException {
        return new Fraction(frac.num * n, frac.den);
    }


    /**
     * Multiplie un entier et une fraction.
     *
     * @param n  un entier.
     * @param frac  une fraction.
     *
     * @return le produit de l'entier et de la fraction.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction mult(int n, Fraction frac) throws AnalyseException {
        return new Fraction(frac.num * n, frac.den);
    }


    /**
     * Multiplie deux fractions.
     *
     * @param f1  une fraction.
     * @param f2  une fraction.
     *
     * @return le produit des deux fractions.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction mult(Fraction f1, Fraction f2) throws AnalyseException {
        return new Fraction(f1.num * f2.num, f1.den * f2.den);
    }


    
    

    /**
     * Divise une fraction par un entier.
     *
     * @param f  une fraction.
     * @param n  un entier.
     *
     * @return le quotient de la fraction par l'entier.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction div(Fraction f, int n) throws AnalyseException {
        return new Fraction(f.num, n * f.den);
    }


    /**
     * Divise un entier par une fraction.
     *
     * @param n  un entier.
     * @param f  une fraction.
     *
     * @return le quotient de l'entier par la fraction.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction div(int n, Fraction f) throws AnalyseException {
        return new Fraction(n * f.den, f.num);
    }


    /**
     * Divise deux fractions.
     *
     * @param f1  une fraction.
     * @param f2  une fraction.
     *
     * @return le quotient des deux fractions: <code>f1/f2</code>.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction div(Fraction f1, Fraction f2) throws AnalyseException {
        return new Fraction(f1.num * f2.den , f1.den * f2.num);
    }



    /**
     * Prend la puissance d'une fraction.
     *
     * @param f  une fraction.
     * @param n  la puisance
     *
     * @return la puisance <code>f^n</code>.
     *
     * @throws AnalyseException  si l'opération n'est mathématiquement pas possible.
     */
    public static Fraction pow(Fraction f, int n) throws AnalyseException {
        return new Fraction(Variable.intpower(f.num, n), Variable.intpower(f.den, n));
    }
    

}
